#ifndef PLINIA_VIEW_H
#define PLINIA_VIEW_H
#include <gtk/gtk.h>
#include "types.h"
#include "window/window.h"

void plinia_view_show_toolbar(GSimpleAction *action, GVariant *state, PliniaWindow *window);

#endif
