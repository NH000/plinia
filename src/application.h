#ifndef PLINIA_APPLICATION_H
#define PLINIA_APPLICATION_H
#include <gtk/gtk.h>
#include "document/document.h"
#include "types.h"

#define PLINIA_TYPE_APPLICATION plinia_application_get_type()
G_DECLARE_FINAL_TYPE(PliniaApplication, plinia_application, PLINIA, APPLICATION, GtkApplication)

PliniaApplication *plinia_application_new(const char *application_id);
void plinia_application_set_document(PliniaApplication *application, PliniaDocument *document);
PliniaDocument *plinia_application_get_document(PliniaApplication *application);
const PliniaApplicationOptions *plinia_application_options_get(PliniaApplication *application);
void plinia_application_options_set(PliniaApplication *application, const PliniaApplicationOptions *application_options);
void plinia_application_options_update(PliniaApplication *application);

#endif
