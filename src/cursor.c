#include <glib/gi18n.h>
#include "cursor.h"

typedef struct
{
    GdkDisplay *display;    // Display on which the supplied window is located.
    GdkCursor *cursor;      // Underlying cursor object.
} PliniaCursorPrivate;

struct _PliniaCursor
{
    GObject parent_instance;

    GdkWindow *window;  // Window whose cursor to control.
    char *cursor_name;  // Name of the currently active cursor.
};

G_DEFINE_TYPE_WITH_PRIVATE(PliniaCursor, plinia_cursor, G_TYPE_OBJECT)

typedef enum
{
    PROP_WINDOW = 1,
    PROP_CURSOR_NAME,
    N_PROPERTIES
} PliniaCursorProperty;

static GParamSpec *properties[N_PROPERTIES] = {NULL};

static void plinia_cursor_set_property(GObject *g_object, guint property_id, const GValue *value, GParamSpec *pspec)
{
    PliniaCursor *instance = PLINIA_CURSOR(g_object);
    PliniaCursorPrivate *private = plinia_cursor_get_instance_private(instance);

    switch (property_id)
    {
        case PROP_WINDOW:
            if (instance->window)
                g_object_remove_weak_pointer(G_OBJECT(instance->window), (gpointer *) &(instance->window));

            if ((instance->window = g_value_get_object(value)))
                g_object_add_weak_pointer(G_OBJECT(instance->window), (gpointer *) &(instance->window));
            break;
        case PROP_CURSOR_NAME:
            if (instance->window)
            {
                const char *cursor_name = g_value_get_string(value);
                GdkCursor *cursor = cursor_name ? gdk_cursor_new_from_name(private->display, cursor_name) : NULL;

                if (cursor || !cursor_name)
                {
                    g_free(instance->cursor_name);
                    instance->cursor_name = g_strdup(cursor_name);

                    if (private->cursor)
                        g_object_unref(private->cursor);

                    private->cursor = cursor;

                    // Finally set cursor of the window.
                    gdk_window_set_cursor(instance->window, private->cursor);
                }
            }
            else
                g_critical("Referenced window does not exist.");
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(g_object, property_id, pspec);
    }
}

static void plinia_cursor_get_property(GObject *g_object, guint property_id, GValue *value, GParamSpec *pspec)
{
    PliniaCursor *instance = PLINIA_CURSOR(g_object);

    switch (property_id)
    {
        case PROP_WINDOW:
            g_value_set_object(value, instance->window);
            break;
        case PROP_CURSOR_NAME:
            g_value_set_string(value, instance->cursor_name);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(g_object, property_id, pspec);
    }
}

static void plinia_cursor_constructed(GObject *g_object)
{
    PliniaCursor *instance = PLINIA_CURSOR(g_object);
    PliniaCursorPrivate *private = plinia_cursor_get_instance_private(instance);

    private->display = instance->window ? gdk_window_get_display(instance->window) : NULL;

    G_OBJECT_CLASS(plinia_cursor_parent_class)->constructed(g_object);
}

static void plinia_cursor_finalize(GObject *g_object)
{
    PliniaCursor *instance = PLINIA_CURSOR(g_object);
    PliniaCursorPrivate *private = plinia_cursor_get_instance_private(instance);

    if (private->cursor)
        g_object_unref(private->cursor);
    g_free(instance->cursor_name);

    if (instance->window)
        g_object_remove_weak_pointer(G_OBJECT(instance->window), (gpointer *) &(instance->window));

    G_OBJECT_CLASS(plinia_cursor_parent_class)->finalize(g_object);
}

static void plinia_cursor_class_init(PliniaCursorClass *class)
{
    GObjectClass *g_object_class = G_OBJECT_CLASS(class);
    g_object_class->set_property = plinia_cursor_set_property;
    g_object_class->get_property = plinia_cursor_get_property;
    g_object_class->constructed = plinia_cursor_constructed;
    g_object_class->finalize = plinia_cursor_finalize;

    properties[PROP_WINDOW] = g_param_spec_object("window", _("Window"), _("Window whose cursor is controlled."), GDK_TYPE_WINDOW, G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
    properties[PROP_CURSOR_NAME] = g_param_spec_string("cursor-name", _("Cursor name"), _("Name of the currently active cursor."), NULL, G_PARAM_READWRITE);

    g_object_class_install_properties(g_object_class, N_PROPERTIES, properties);
}

static void plinia_cursor_init(PliniaCursor *instance)
{
}

/* API */

PliniaCursor *plinia_cursor_new(GdkWindow *window)
{
    g_return_val_if_fail(GDK_IS_WINDOW(window), NULL);

    return g_object_new(PLINIA_TYPE_CURSOR, "window", window, NULL);
}

GdkWindow *plinia_cursor_get_window(PliniaCursor *cursor)
{
    g_return_val_if_fail(PLINIA_IS_CURSOR(cursor), NULL);

    GdkWindow *window;
    g_object_get(cursor, "window", &window, NULL);

    return window;
}

void plinia_cursor_set_cursor_name(PliniaCursor *cursor, const char *cursor_name)
{
    g_return_if_fail(PLINIA_IS_CURSOR(cursor));

    g_object_set(cursor, "cursor-name", cursor_name, NULL);
}

const char *plinia_cursor_get_cursor_name(PliniaCursor *cursor)
{
    g_return_val_if_fail(PLINIA_IS_CURSOR(cursor), NULL);

    const char *cursor_name;
    g_object_get(cursor, "cursor-name", &cursor_name, NULL);

    return cursor_name;
}
