#ifndef PLINIA_INFO_H
#define PLINIA_INFO_H
#include <gtk/gtk.h>

// Program information.
#define PLINIA_INFO_NAME        "Plinia"                            // User-displayable name.
#define PLINIA_INFO_VERS        "0.5"                               // Version of the program.
#define PLINIA_INFO_LICE        "GPL3"                              // License of the program.
#define PLINIA_INFO_LICE_TYPE   GTK_LICENSE_GPL_3_0                 // License of the program as enumeration value.
#define PLINIA_INFO_SITE        "https://gitlab.com/NH000/plinia"   // Program's website.
#define PLINIA_INFO_AUTH        "Nikola Hadžić"                     // Author of the program.
#define PLINIA_INFO_AUTH_EMAIL  "nikola.hadzic.000@protonmail.com"  // Email of the author.
#define PLINIA_INFO_YEAR        "2018-2022"                         // Years during which there was work on the program.

#endif
