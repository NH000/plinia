#include <glib/gi18n.h>
#include "toolbar.h"
#include "separator_invisible_tool_item.h"
#include "spacer_tool_item.h"
#include "navigator.h"
#include "zoom_level_chooser.h"

typedef struct
{
    GtkToolButton *prev_page;                   // "win.go.prev-page" action.
    GtkToolButton *next_page;                   // "win.go.next-page" action.
    PliniaNavigator *navigator;                 // Navigate the document.
    GtkToggleToolButton *show_sidebar;          // "win.view.show-sidebar" action.
    PliniaZoomLevelChooser *zoom_level_chooser; // Set the zoom level.
} PliniaToolbarGUI;

typedef struct
{
    PliniaToolbarGUI gui;
} PliniaToolbarPrivate;

struct _PliniaToolbar
{
    GtkToolbar parent_instance;
};

G_DEFINE_TYPE_WITH_PRIVATE(PliniaToolbar, plinia_toolbar, GTK_TYPE_TOOLBAR)

static void plinia_toolbar_class_init(PliniaToolbarClass *class)
{
}

static void plinia_toolbar_init(PliniaToolbar *instance)
{
    PliniaToolbarPrivate *private = plinia_toolbar_get_instance_private(instance);

    /* GUI CREATION */

    private->gui.prev_page = GTK_TOOL_BUTTON(gtk_tool_button_new(gtk_image_new_from_icon_name("go-previous", GTK_ICON_SIZE_SMALL_TOOLBAR), NULL));
    private->gui.next_page = GTK_TOOL_BUTTON(gtk_tool_button_new(gtk_image_new_from_icon_name("go-next", GTK_ICON_SIZE_SMALL_TOOLBAR), NULL));
    private->gui.navigator = PLINIA_NAVIGATOR(plinia_navigator_new());
    private->gui.show_sidebar = GTK_TOGGLE_TOOL_BUTTON(gtk_toggle_tool_button_new());
    private->gui.zoom_level_chooser = PLINIA_ZOOM_LEVEL_CHOOSER(plinia_zoom_level_chooser_new(PLINIA_ZOOM_LEVEL_MIN, PLINIA_ZOOM_LEVEL_MAX, PLINIA_ZOOM_LEVEL_DEF));

    gtk_tool_button_set_icon_widget(GTK_TOOL_BUTTON(private->gui.show_sidebar), gtk_image_new_from_icon_name("view-left-pane", GTK_ICON_SIZE_SMALL_TOOLBAR));

    gtk_widget_set_tooltip_text(GTK_WIDGET(private->gui.prev_page), _("Go to the previous page"));
    gtk_widget_set_tooltip_text(GTK_WIDGET(private->gui.next_page), _("Go to the next page"));
    gtk_widget_set_tooltip_text(GTK_WIDGET(private->gui.navigator), _("Navigate the document"));
    gtk_widget_set_tooltip_text(GTK_WIDGET(private->gui.show_sidebar), _("Toggle the sidebar"));
    gtk_widget_set_tooltip_text(GTK_WIDGET(private->gui.zoom_level_chooser), _("Set the zoom level"));

    GtkToolbar *gtk_toolbar = GTK_TOOLBAR(instance);
    gtk_toolbar_insert(gtk_toolbar, GTK_TOOL_ITEM(private->gui.prev_page), -1);
    gtk_toolbar_insert(gtk_toolbar, GTK_TOOL_ITEM(private->gui.next_page), -1);
    gtk_toolbar_insert(gtk_toolbar, plinia_separator_invisible_tool_item_new(), -1);
    gtk_toolbar_insert(gtk_toolbar, GTK_TOOL_ITEM(private->gui.navigator), -1);
    gtk_toolbar_insert(gtk_toolbar, gtk_separator_tool_item_new(), -1);
    gtk_toolbar_insert(gtk_toolbar, GTK_TOOL_ITEM(private->gui.show_sidebar), -1);
    gtk_toolbar_insert(gtk_toolbar, plinia_spacer_tool_item_new(), -1);
    gtk_toolbar_insert(gtk_toolbar, GTK_TOOL_ITEM(private->gui.zoom_level_chooser), -1);

    gtk_actionable_set_action_name(GTK_ACTIONABLE(private->gui.prev_page), "win.go.prev-page");
    gtk_actionable_set_action_name(GTK_ACTIONABLE(private->gui.next_page), "win.go.next-page");
    gtk_actionable_set_action_name(GTK_ACTIONABLE(private->gui.show_sidebar), "win.view.show-sidebar");
}

/* API */

GtkWidget *plinia_toolbar_new()
{
    return g_object_new(PLINIA_TYPE_TOOLBAR, NULL);
}
