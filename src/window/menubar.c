#include <glib/gi18n.h>
#include "menubar.h"
#include "../view.h"

struct _PliniaMenubar
{
    GtkMenuBar parent_instance;
};

G_DEFINE_TYPE(PliniaMenubar, plinia_menubar, GTK_TYPE_MENU_BAR)

static void plinia_menubar_class_init(PliniaMenubarClass *class)
{
}

static void plinia_menubar_init(PliniaMenubar *instance)
{
    /* MENU MODEL */

    GMenu *model = g_menu_new();

#define PLINIA_MENUBAR_ADD_MENU_ITEM_NUM(menu, label, action, num)  {                                                           \
                                                                        char *action_num = g_strdup_printf(action "(%d)", num); \
                                                                        g_menu_append(menu, label, action_num);                 \
                                                                        g_free(action_num);                                     \
                                                                    }

    // "File" submenu.
    {
        GMenu *model_submenu_file_section_open = g_menu_new();
        g_menu_append(model_submenu_file_section_open, _("_Open\u2026"), "file.open");
        g_menu_append(model_submenu_file_section_open, _("Op_en A Copy"), "file.open-a-copy");

        GMenu *model_submenu_file_section_save = g_menu_new();
        g_menu_append(model_submenu_file_section_save, _("_Save A Copy\u2026"), "file.save-a-copy");

        GMenu *model_submenu_file_section_reload = g_menu_new();
        g_menu_append(model_submenu_file_section_reload, _("_Reload"), "file.reload");

        GMenu *model_submenu_file_section_properties = g_menu_new();
        g_menu_append(model_submenu_file_section_properties, _("_Properties\u2026"), "file.properties");

        GMenu *model_submenu_file_section_close = g_menu_new();
        g_menu_append(model_submenu_file_section_close, _("_Close"), "file.close");

        GMenu *model_submenu_file = g_menu_new();
        g_menu_append_section(model_submenu_file, NULL, G_MENU_MODEL(model_submenu_file_section_open));
        g_menu_append_section(model_submenu_file, NULL, G_MENU_MODEL(model_submenu_file_section_save));
        g_menu_append_section(model_submenu_file, NULL, G_MENU_MODEL(model_submenu_file_section_reload));
        g_menu_append_section(model_submenu_file, NULL, G_MENU_MODEL(model_submenu_file_section_properties));
        g_menu_append_section(model_submenu_file, NULL, G_MENU_MODEL(model_submenu_file_section_close));

        g_menu_append_submenu(model, _("_File"), G_MENU_MODEL(model_submenu_file));
    }

    // "Edit" submenu.
    {
        GMenu *model_submenu_edit_section_find = g_menu_new();
        g_menu_append(model_submenu_edit_section_find, _("_Find\u2026"), "edit.find");
        g_menu_append(model_submenu_edit_section_find, _("Find _Previous"), "edit.find-prev");
        g_menu_append(model_submenu_edit_section_find, _("Find _Next"), "edit.find-next");

        GMenu *model_submenu_edit_section_rotate = g_menu_new();
        g_menu_append(model_submenu_edit_section_rotate, _("Rotate _Left"), "edit.rotate-left");
        g_menu_append(model_submenu_edit_section_rotate, _("Rotate _Right"), "edit.rotate-right");

        GMenu *model_submenu_edit_section_copy = g_menu_new();
        g_menu_append(model_submenu_edit_section_copy, _("_Copy"), "edit.copy");

        GMenu *model_submenu_edit_section_select_all = g_menu_new();
        g_menu_append(model_submenu_edit_section_select_all, _("Select _All"), "edit.select-all");

        GMenu *model_submenu_edit_section_add_bookmark = g_menu_new();
        g_menu_append(model_submenu_edit_section_add_bookmark, _("Add _Bookmark"), "edit.add-bookmark");

        GMenu *model_submenu_edit_section_save_current_settings_as_default = g_menu_new();
        g_menu_append(model_submenu_edit_section_save_current_settings_as_default, _("_Save Current Settings As Default"), "edit.save-current-settings-as-default");

        GMenu *model_submenu_edit = g_menu_new();
        g_menu_append_section(model_submenu_edit, NULL, G_MENU_MODEL(model_submenu_edit_section_find));
        g_menu_append_section(model_submenu_edit, NULL, G_MENU_MODEL(model_submenu_edit_section_rotate));
        g_menu_append_section(model_submenu_edit, NULL, G_MENU_MODEL(model_submenu_edit_section_copy));
        g_menu_append_section(model_submenu_edit, NULL, G_MENU_MODEL(model_submenu_edit_section_select_all));
        g_menu_append_section(model_submenu_edit, NULL, G_MENU_MODEL(model_submenu_edit_section_add_bookmark));
        g_menu_append_section(model_submenu_edit, NULL, G_MENU_MODEL(model_submenu_edit_section_save_current_settings_as_default));

        g_menu_append_submenu(model, _("_Edit"), G_MENU_MODEL(model_submenu_edit));
    }

    // "View" submenu.
    {
        GMenu *model_submenu_view_section_show = g_menu_new();
        g_menu_append(model_submenu_view_section_show, _("Show _Toolbar"), "view.show-toolbar");
        g_menu_append(model_submenu_view_section_show, _("Show _Sidebar"), "view.show-sidebar");

        GMenu *model_submenu_view_section_continuous = g_menu_new();
        g_menu_append(model_submenu_view_section_continuous, _("_Continuous"), "view.continuous");

        GMenu *model_submenu_view_section_dual = g_menu_new();

        // "View -> Dual" submenu.
        {
            GMenu *model_submenu_view_section_dual_submenu_dual_section_dual = g_menu_new();
            PLINIA_MENUBAR_ADD_MENU_ITEM_NUM(model_submenu_view_section_dual_submenu_dual_section_dual, _("Off"), "view.dual", PLINIA_VIEW_DUAL_MODE_OFF)
            PLINIA_MENUBAR_ADD_MENU_ITEM_NUM(model_submenu_view_section_dual_submenu_dual_section_dual, _("Even Pages Left"), "view.dual", PLINIA_VIEW_DUAL_MODE_EVEN_PAGES_LEFT)
            PLINIA_MENUBAR_ADD_MENU_ITEM_NUM(model_submenu_view_section_dual_submenu_dual_section_dual, _("Even Pages Right"), "view.dual", PLINIA_VIEW_DUAL_MODE_EVEN_PAGES_RIGHT)

            GMenu *model_submenu_view_section_dual_submenu_dual_section_direction = g_menu_new();

            // "View -> Dual -> Direction" submenu.
            {
                GMenu *model_submenu_view_section_dual_submenu_dual_section_direction_submenu_direction_section_direction = g_menu_new();
                PLINIA_MENUBAR_ADD_MENU_ITEM_NUM(model_submenu_view_section_dual_submenu_dual_section_direction_submenu_direction_section_direction, _("Left To Right"), "view.dual.direction", PLINIA_VIEW_DUAL_DIRECTION_LEFT_TO_RIGHT)
                PLINIA_MENUBAR_ADD_MENU_ITEM_NUM(model_submenu_view_section_dual_submenu_dual_section_direction_submenu_direction_section_direction, _("Right To Left"), "view.dual.direction", PLINIA_VIEW_DUAL_DIRECTION_RIGHT_TO_LEFT)

                GMenu *model_submenu_view_section_dual_submenu_dual_section_direction_submenu_direction = g_menu_new();
                g_menu_append_section(model_submenu_view_section_dual_submenu_dual_section_direction_submenu_direction, NULL, G_MENU_MODEL(model_submenu_view_section_dual_submenu_dual_section_direction_submenu_direction_section_direction));

                g_menu_append_submenu(model_submenu_view_section_dual_submenu_dual_section_direction, _("_Direction"), G_MENU_MODEL(model_submenu_view_section_dual_submenu_dual_section_direction_submenu_direction));
            }

            GMenu *model_submenu_view_section_dual_submenu_dual = g_menu_new();
            g_menu_append_section(model_submenu_view_section_dual_submenu_dual, NULL, G_MENU_MODEL(model_submenu_view_section_dual_submenu_dual_section_dual));
            g_menu_append_section(model_submenu_view_section_dual_submenu_dual, NULL, G_MENU_MODEL(model_submenu_view_section_dual_submenu_dual_section_direction));

            g_menu_append_submenu(model_submenu_view_section_dual, _("_Dual"), G_MENU_MODEL(model_submenu_view_section_dual_submenu_dual));
        }

        GMenu *model_submenu_view_section_inverted_colors = g_menu_new();
        g_menu_append(model_submenu_view_section_inverted_colors, _("I_nverted Colors"), "view.inverted-colors");

        GMenu *model_submenu_view_section_zoom = g_menu_new();
        g_menu_append(model_submenu_view_section_zoom, _("Zoom _In"), "view.zoom-in");
        g_menu_append(model_submenu_view_section_zoom, _("Zoom _Out"), "view.zoom-out");
        g_menu_append(model_submenu_view_section_zoom, _("Zoom _Reset"), "view.zoom-reset");

        GMenu *model_submenu_view_section_fit = g_menu_new();
        g_menu_append(model_submenu_view_section_fit, _("Fit _Width"), "view.fit-width");
        g_menu_append(model_submenu_view_section_fit, _("Fit _Height"), "view.fit-height");

        GMenu *model_submenu_view_section_resize_window_to_fit = g_menu_new();
        g_menu_append(model_submenu_view_section_resize_window_to_fit, _("R_esize Window To Fit"), "view.resize-window-to-fit");

        GMenu *model_submenu_view_section_preview_mode = g_menu_new();
        g_menu_append(model_submenu_view_section_preview_mode, _("_Fullscreen"), "view.fullscreen");
        g_menu_append(model_submenu_view_section_preview_mode, _("_Presentation"), "view.presentation");

        GMenu *model_submenu_view = g_menu_new();
        g_menu_append_section(model_submenu_view, NULL, G_MENU_MODEL(model_submenu_view_section_show));
        g_menu_append_section(model_submenu_view, NULL, G_MENU_MODEL(model_submenu_view_section_continuous));
        g_menu_append_section(model_submenu_view, NULL, G_MENU_MODEL(model_submenu_view_section_dual));
        g_menu_append_section(model_submenu_view, NULL, G_MENU_MODEL(model_submenu_view_section_inverted_colors));
        g_menu_append_section(model_submenu_view, NULL, G_MENU_MODEL(model_submenu_view_section_zoom));
        g_menu_append_section(model_submenu_view, NULL, G_MENU_MODEL(model_submenu_view_section_fit));
        g_menu_append_section(model_submenu_view, NULL, G_MENU_MODEL(model_submenu_view_section_resize_window_to_fit));
        g_menu_append_section(model_submenu_view, NULL, G_MENU_MODEL(model_submenu_view_section_preview_mode));

        g_menu_append_submenu(model, _("_View"), G_MENU_MODEL(model_submenu_view));
    }

    // "Go" submenu.
    {
        GMenu *model_submenu_go_section_near = g_menu_new();
        g_menu_append(model_submenu_go_section_near, _("_Previous Page"), "go.prev-page");
        g_menu_append(model_submenu_go_section_near, _("_Next Page"), "go.next-page");

        GMenu *model_submenu_go_section_far = g_menu_new();
        g_menu_append(model_submenu_go_section_far, _("_First Page"), "go.first-page");
        g_menu_append(model_submenu_go_section_far, _("_Last Page"), "go.last-page");

        GMenu *model_submenu_go_section_jump = g_menu_new();
        g_menu_append(model_submenu_go_section_jump, _("_Jump To Page\u2026"), "go.jump-to-page");

        GMenu *model_submenu_go = g_menu_new();
        g_menu_append_section(model_submenu_go, NULL, G_MENU_MODEL(model_submenu_go_section_near));
        g_menu_append_section(model_submenu_go, NULL, G_MENU_MODEL(model_submenu_go_section_far));
        g_menu_append_section(model_submenu_go, NULL, G_MENU_MODEL(model_submenu_go_section_jump));

        g_menu_append_submenu(model, _("_Go"), G_MENU_MODEL(model_submenu_go));
    }

    // "History" submenu.
    {
        GMenu *model_submenu_history_section_navigate = g_menu_new();
        g_menu_append(model_submenu_history_section_navigate, _("_Backward"), "history.backward");
        g_menu_append(model_submenu_history_section_navigate, _("_Forward"), "history.forward");

        GMenu *model_submenu_history = g_menu_new();
        g_menu_append_section(model_submenu_history, NULL, G_MENU_MODEL(model_submenu_history_section_navigate));

        g_menu_append_submenu(model, _("H_istory"), G_MENU_MODEL(model_submenu_history));
    }

    // "Help" submenu.
    {
        GMenu *model_submenu_help_section_about = g_menu_new();
        g_menu_append(model_submenu_help_section_about, _("_About"), "help.about");

        GMenu *model_submenu_help = g_menu_new();
        g_menu_append_section(model_submenu_help, NULL, G_MENU_MODEL(model_submenu_help_section_about));

        g_menu_append_submenu(model, _("_Help"), G_MENU_MODEL(model_submenu_help));
    }

    // Bind menubar to menu model.
    gtk_menu_shell_bind_model(GTK_MENU_SHELL(instance), G_MENU_MODEL(model), "win", FALSE);

    g_object_unref(model);

#undef PLINIA_MENUBAR_ADD_MENU_ITEM_NUM
}

/* API */

GtkWidget *plinia_menubar_new()
{
    return g_object_new(PLINIA_TYPE_MENUBAR, NULL);
}
