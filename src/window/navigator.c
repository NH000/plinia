#include <glib/gi18n.h>
#include "navigator.h"
#include "../auxillary.h"

typedef struct
{
    GtkBox *content_area;   // Content area.
    GtkEntry *current_page; // Entry that contains the current page number/title.
    GtkEntry *total_pages;  // Entry that contains the total number of pages. (It is disabled.)
} PliniaNavigatorGUI;

typedef struct
{
    PliniaNavigatorGUI gui;         // GUI elements.
} PliniaNavigatorPrivate;

struct _PliniaNavigator
{
    GtkToolItem parent_instance;

    guint current_page;     // Number of the current page in the document.
    guint total_pages;      // Total pages of the document.
};

G_DEFINE_TYPE_WITH_PRIVATE(PliniaNavigator, plinia_navigator, GTK_TYPE_TOOL_ITEM)

#define PLINIA_NAVIGATOR_SET_CURRENT_PAGE_ENTRY_WIDTH(instance, private)    if (gtk_entry_get_completion(private->gui.current_page))                                                                \
                                                                                gtk_entry_set_width_chars(private->gui.current_page, 10);                                                           \
                                                                            else                                                                                                                    \
                                                                            {                                                                                                                       \
                                                                                char * const current_page_string = g_strdup_printf("%u", instance->total_pages);                                    \
                                                                                gtk_entry_set_width_chars(private->gui.current_page, plinia_auxillary_get_utf8_string_length(current_page_string)); \
                                                                                g_free(current_page_string);                                                                                        \
                                                                            }
#define PLINIA_NAVIGATOR_SET_TOTAL_PAGES_ENTRY_CONTENTS(instance, private)  {                                                                                            \
                                                                                char * const total_pages_string = g_strdup_printf(_("of %u"), instance->total_pages);    \
                                                                                gtk_entry_set_text(private->gui.total_pages, total_pages_string);                        \
                                                                                g_free(total_pages_string);                                                              \
                                                                            }
#define PLINIA_NAVIGATOR_SET_TOTAL_PAGES_ENTRY_WIDTH(instance, private) {                                                                                                                       \
                                                                            char * const total_pages_string = g_strdup_printf(_("(%u of %u)"), instance->total_pages, instance->total_pages);   \
                                                                            gtk_entry_set_width_chars(private->gui.total_pages, plinia_auxillary_get_utf8_string_length(total_pages_string));   \
                                                                            g_free(total_pages_string);                                                                                         \
                                                                        }

typedef enum
{
    SIG_ACTIVATE,
    N_SIGNALS
} PliniaNavigateSignal;

static guint signals[N_SIGNALS];

typedef enum
{
    PROP_CURRENT_PAGE = 1,
    PROP_TOTAL_PAGES,
    N_PROPERTIES
} PliniaNavigatorProperty;

static GParamSpec *properties[N_PROPERTIES] = {NULL};

static void plinia_navigator_set_property(GObject *g_object, guint property_id, const GValue *value, GParamSpec *pspec)
{
    PliniaNavigator *instance = PLINIA_NAVIGATOR(g_object);
    PliniaNavigatorPrivate *private = plinia_navigator_get_instance_private(instance);

    switch (property_id)
    {
        case PROP_CURRENT_PAGE:
            if (g_value_get_uint(value) > instance->total_pages)
                g_critical("Tried to set the current page number to a value greater than the total pages number");
            else
            {
                instance->current_page = g_value_get_uint(value);
                PLINIA_NAVIGATOR_SET_CURRENT_PAGE_ENTRY_WIDTH(instance, private)
            }
            break;
        case PROP_TOTAL_PAGES:
            if (g_value_get_uint(value) < instance->current_page)
                g_critical("Tried to set the total number of pages to a value lesser than the current page number");
            else
            {
                instance->total_pages = g_value_get_uint(value);
                PLINIA_NAVIGATOR_SET_TOTAL_PAGES_ENTRY_WIDTH(instance, private)
                PLINIA_NAVIGATOR_SET_TOTAL_PAGES_ENTRY_CONTENTS(instance, private)
            }
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(g_object, property_id, pspec);
    }
}

static void plinia_navigator_class_init(PliniaNavigatorClass *class)
{
    GObjectClass *g_object_class = G_OBJECT_CLASS(class);
    g_object_class->set_property = plinia_navigator_set_property;

    signals[SIG_ACTIVATE] = g_signal_new("activate", PLINIA_TYPE_NAVIGATOR, G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL, G_TYPE_NONE, 6,
        G_TYPE_UINT,    // Page number.
        G_TYPE_DOUBLE,  // Left page coordinate.
        G_TYPE_DOUBLE,  // Bottom page coordinate.
        G_TYPE_DOUBLE,  // Right page coordinate.
        G_TYPE_DOUBLE,  // Top page coordinate.
        G_TYPE_DOUBLE   // Zoom level.
        );

    properties[PROP_CURRENT_PAGE] = g_param_spec_uint("current-page", _("Current page"), _("Number of the current document page."), 1, G_MAXUINT, 1, G_PARAM_WRITABLE);
    properties[PROP_TOTAL_PAGES] = g_param_spec_uint("total-pages", _("Total pages"), _("Total number of pages in the document."), 1, G_MAXUINT, 1, G_PARAM_WRITABLE);

    g_object_class_install_properties(g_object_class, N_PROPERTIES, properties);
}

static void plinia_navigator_init(PliniaNavigator *instance)
{
    PliniaNavigatorPrivate *private = plinia_navigator_get_instance_private(instance);

    /* INITIALING PROPERTIES */

    instance->current_page = 1;
    instance->total_pages = 1;

    /* GUI CREATION */

    // Create and set properties of the current page entry.
    private->gui.current_page = GTK_ENTRY(gtk_entry_new());
    gtk_widget_add_events(GTK_WIDGET(private->gui.current_page), GDK_SCROLL_MASK);
    gtk_entry_set_input_hints(private->gui.current_page, GTK_INPUT_HINT_NO_EMOJI);
    PLINIA_NAVIGATOR_SET_CURRENT_PAGE_ENTRY_WIDTH(instance, private)

    // Create and set properties of the total pages entry.
    private->gui.total_pages = GTK_ENTRY(gtk_entry_new());
    gtk_widget_set_sensitive(GTK_WIDGET(private->gui.total_pages), FALSE);
    PLINIA_NAVIGATOR_SET_TOTAL_PAGES_ENTRY_WIDTH(instance, private)
    PLINIA_NAVIGATOR_SET_TOTAL_PAGES_ENTRY_CONTENTS(instance, private)

    // Create content area and set its properties.
    private->gui.content_area = GTK_BOX(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0));
    GtkStyleContext *content_area_style_context = gtk_widget_get_style_context(GTK_WIDGET(private->gui.content_area));
    gtk_style_context_add_class(content_area_style_context, GTK_STYLE_CLASS_RAISED);
    gtk_style_context_add_class(content_area_style_context, GTK_STYLE_CLASS_LINKED);

    // Attach page entries to the content area.
    gtk_box_pack_start(private->gui.content_area, GTK_WIDGET(private->gui.current_page), FALSE, FALSE, 0);
    gtk_box_pack_start(private->gui.content_area, GTK_WIDGET(private->gui.total_pages), FALSE, FALSE, 0);

    // Add content area to the navigator widget itself.
    gtk_container_add(GTK_CONTAINER(instance), GTK_WIDGET(private->gui.content_area));
}

/* API */

GtkToolItem *plinia_navigator_new()
{
    return g_object_new(PLINIA_TYPE_NAVIGATOR, NULL);
}

void plinia_navigator_initialize(PliniaNavigator *navigator, guint total_pages, GtkTreeModel *completion_model)
{
    g_return_if_fail(PLINIA_IS_NAVIGATOR(navigator));
    g_return_if_fail(total_pages > 0);
    g_return_if_fail(completion_model == NULL || GTK_IS_TREE_MODEL(completion_model));

    PliniaNavigatorPrivate *private = plinia_navigator_get_instance_private(navigator);

    g_object_set(navigator, "current-page", 1, "total-pages", total_pages, NULL);

    if (completion_model)
    {
        // Create completion object and set its properties.
        GtkEntryCompletion *completion = gtk_entry_completion_new();
        g_object_set(completion, "model", completion_model, "popup-set-width", FALSE, "text-column", 0, NULL);

        // Add completion object to the current page entry.
        gtk_entry_set_completion(private->gui.current_page, completion);
    }
    else
    {
        // Remove completion from the current page entry.
        gtk_entry_set_completion(private->gui.current_page, NULL);
    }

    PLINIA_NAVIGATOR_SET_CURRENT_PAGE_ENTRY_WIDTH(navigator, private)
    // TODO: Set current page entry contents.
    PLINIA_NAVIGATOR_SET_TOTAL_PAGES_ENTRY_WIDTH(navigator, private)
    PLINIA_NAVIGATOR_SET_TOTAL_PAGES_ENTRY_CONTENTS(navigator, private)
}

void plinia_navigator_set_current_page(PliniaNavigator *navigator, guint current_page)
{
    g_return_if_fail(PLINIA_IS_NAVIGATOR(navigator));
    g_return_if_fail(current_page > 0);

    g_object_set(navigator, "current-page", current_page, NULL);

    // TODO: Set current page entry contents.
}
