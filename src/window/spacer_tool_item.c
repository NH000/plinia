#include "spacer_tool_item.h"

struct _PliniaSpacerToolItem
{
    PliniaSeparatorInvisibleToolItem parent_instance;
};

G_DEFINE_TYPE(PliniaSpacerToolItem, plinia_spacer_tool_item, PLINIA_TYPE_SEPARATOR_INVISIBLE_TOOL_ITEM)

static void plinia_spacer_tool_item_class_init(PliniaSpacerToolItemClass *class)
{
}

static void plinia_spacer_tool_item_init(PliniaSpacerToolItem *instance)
{
    gtk_tool_item_set_expand(GTK_TOOL_ITEM(instance), TRUE);
}

/* API */

GtkToolItem *plinia_spacer_tool_item_new()
{
    return g_object_new(PLINIA_TYPE_SPACER_TOOL_ITEM, NULL);
}
