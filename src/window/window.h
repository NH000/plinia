#ifndef PLINIA_WINDOW_H
#define PLINIA_WINDOW_H
#include <gtk/gtk.h>
#include "../application.h"
#include "menubar.h"
#include "toolbar.h"

#define PLINIA_TYPE_WINDOW  plinia_window_get_type()
G_DECLARE_FINAL_TYPE(PliniaWindow, plinia_window, PLINIA, WINDOW, GtkApplicationWindow)

GtkWidget *plinia_window_new(PliniaApplication *application);
GtkWidget *plinia_window_get_menubar(PliniaWindow *window);
GtkWidget *plinia_window_get_toolbar(PliniaWindow *window);
void plinia_window_add_accelerators(PliniaWindow *window);
void plinia_window_set_file_enabled(PliniaWindow *window, gboolean enabled);
void plinia_window_set_find_prev_and_next_enabled(PliniaWindow *window, gboolean enabled);
void plinia_window_set_copy_enabled(PliniaWindow *window, gboolean enabled);
void plinia_window_set_navigation_enabled(PliniaWindow *window, gboolean first, gboolean last);
void plinia_window_set_bookmark_enabled(PliniaWindow *window, gboolean enabled);
void plinia_window_set_history_enabled(PliniaWindow *window, gboolean bottom, gboolean top);
void plinia_window_configure_from_options(PliniaWindow *window, const PliniaApplicationOptions *application_options);

#endif
