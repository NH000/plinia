#ifndef PLINIA_NAVIGATOR_H
#define PLINIA_NAVIGATOR_H
#include <gtk/gtk.h>

#define PLINIA_TYPE_NAVIGATOR   plinia_navigator_get_type()
G_DECLARE_FINAL_TYPE(PliniaNavigator, plinia_navigator, PLINIA, NAVIGATOR, GtkToolItem)

GtkToolItem *plinia_navigator_new();
void plinia_navigator_initialize(PliniaNavigator *navigator, guint total_pages, GtkTreeModel *completion_model);
void plinia_navigator_set_current_page(PliniaNavigator *navigator, guint current_page);

#endif
