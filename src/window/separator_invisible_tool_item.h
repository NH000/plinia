#ifndef PLINIA_SEPARATOR_INVISIBLE_TOOL_ITEM_H
#define PLINIA_SEPARATOR_INVISIBLE_TOOL_ITEM_H
#include <gtk/gtk.h>

#define PLINIA_TYPE_SEPARATOR_INVISIBLE_TOOL_ITEM plinia_separator_invisible_tool_item_get_type()
G_DECLARE_DERIVABLE_TYPE(PliniaSeparatorInvisibleToolItem, plinia_separator_invisible_tool_item, PLINIA, SEPARATOR_INVISIBLE_TOOL_ITEM, GtkSeparatorToolItem)

struct _PliniaSeparatorInvisibleToolItemClass
{
    GtkSeparatorToolItemClass parent_class;
};

GtkToolItem *plinia_separator_invisible_tool_item_new();

#endif
