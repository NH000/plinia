#ifndef PLINIA_TOOLBAR_H
#define PLINIA_TOOLBAR_H
#include <gtk/gtk.h>

#define PLINIA_TYPE_TOOLBAR plinia_toolbar_get_type()
G_DECLARE_FINAL_TYPE(PliniaToolbar, plinia_toolbar, PLINIA, TOOLBAR, GtkToolbar)

GtkWidget *plinia_toolbar_new();

#endif
