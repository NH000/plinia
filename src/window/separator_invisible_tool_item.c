#include "separator_invisible_tool_item.h"

G_DEFINE_TYPE(PliniaSeparatorInvisibleToolItem, plinia_separator_invisible_tool_item, GTK_TYPE_SEPARATOR_TOOL_ITEM)

static void plinia_separator_invisible_tool_item_class_init(PliniaSeparatorInvisibleToolItemClass *class)
{
}

static void plinia_separator_invisible_tool_item_init(PliniaSeparatorInvisibleToolItem *instance)
{
    g_object_set(instance, "draw", FALSE, NULL);
}

/* API */

GtkToolItem *plinia_separator_invisible_tool_item_new()
{
    return g_object_new(PLINIA_TYPE_SEPARATOR_INVISIBLE_TOOL_ITEM, NULL);
}
