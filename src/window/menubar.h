#ifndef PLINIA_MENUBAR_H
#define PLINIA_MENUBAR_H
#include <gtk/gtk.h>

#define PLINIA_TYPE_MENUBAR plinia_menubar_get_type()
G_DECLARE_FINAL_TYPE(PliniaMenubar, plinia_menubar, PLINIA, MENUBAR, GtkMenuBar)

GtkWidget *plinia_menubar_new();

#endif
