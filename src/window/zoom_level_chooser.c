#include <glib/gi18n.h>
#include "zoom_level_chooser.h"
#include "../auxillary.h"

typedef struct
{
    GtkBox *content_area;           // Content area.
    GtkEntry *current_zoom_level;   // Entry that contains the current zoom level.
    GtkEntry *percentage_entry;     // Entry that displays '%'.
} PliniaZoomLevelChooserGUI;

typedef struct
{
    PliniaZoomLevelChooserGUI gui;  // GUI elements.
} PliniaZoomLevelChooserPrivate;

struct _PliniaZoomLevelChooser
{
    GtkToolItem parent_instance;

    guint min_value;    // Minimum allowed zoom level value.
    guint max_value;    // Maximum allowed zoom level value.
    guint def_value;    // Default zoom level value.
};

G_DEFINE_TYPE_WITH_PRIVATE(PliniaZoomLevelChooser, plinia_zoom_level_chooser, GTK_TYPE_TOOL_ITEM)

#define PLINIA_ZOOM_LEVEL_CHOOSER_SET_CURRENT_ZOOM_LEVEL_ENTRY_WIDTH(instance, private) {                                                                                                                           \
                                                                                           char * const max_value_string = g_strdup_printf("%u", instance->max_value);                                              \
                                                                                           gtk_entry_set_width_chars(private->gui.current_zoom_level, plinia_auxillary_get_utf8_string_length(max_value_string));   \
                                                                                           g_free(max_value_string);                                                                                                \
                                                                                        }

typedef enum
{
    SIG_ACTIVATE,
    N_SIGNALS
} PliniaZoomLevelChooserSignal;

static guint signals[N_SIGNALS];

typedef enum
{
    PROP_MIN_VALUE = 1,
    PROP_MAX_VALUE,
    PROP_DEF_VALUE,
    N_PROPERTIES
} PliniaZoomLevelChooserProperty;

static GParamSpec *properties[N_PROPERTIES] = {NULL};

static void plinia_zoom_level_chooser_set_property(GObject *g_object, guint property_id, const GValue *value, GParamSpec *pspec)
{
    PliniaZoomLevelChooser *instance = PLINIA_ZOOM_LEVEL_CHOOSER(g_object);
    PliniaZoomLevelChooserPrivate *private = plinia_zoom_level_chooser_get_instance_private(instance);

    switch (property_id)
    {
        case PROP_MIN_VALUE:
            if (g_value_get_uint(value) > instance->max_value)
                g_critical("Tried to set minimum zoom level value to a value greater than the maximum allowed zoom level value");
            else
                instance->min_value = g_value_get_uint(value);
            break;
        case PROP_MAX_VALUE:
            if (g_value_get_uint(value) < instance->min_value)
                g_critical("Tried to set maximum zoom level value to a value lesser than the minimum allowed zoom level value");
            else
            {
                instance->max_value = g_value_get_uint(value);
                PLINIA_ZOOM_LEVEL_CHOOSER_SET_CURRENT_ZOOM_LEVEL_ENTRY_WIDTH(instance, private)
            }
            break;
        case PROP_DEF_VALUE:
            if (g_value_get_uint(value) < instance->min_value || g_value_get_uint(value) > instance->max_value)
                g_critical("Tried to set default zoom level value to a value that is not inbetween minimum and maximum allowed zoom level values");
            else
                instance->def_value = g_value_get_uint(value);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(g_object, property_id, pspec);
    }
}

static void plinia_zoom_level_chooser_get_property(GObject *g_object, guint property_id, GValue *value, GParamSpec *pspec)
{
    PliniaZoomLevelChooser *instance = PLINIA_ZOOM_LEVEL_CHOOSER(g_object);

    switch (property_id)
    {
        case PROP_MIN_VALUE:
            g_value_set_uint(value, instance->min_value);
            break;
        case PROP_MAX_VALUE:
            g_value_set_uint(value, instance->max_value);
            break;
        case PROP_DEF_VALUE:
            g_value_set_uint(value, instance->def_value);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(g_object, property_id, pspec);
    }
}

static void plinia_zoom_level_chooser_class_init(PliniaZoomLevelChooserClass *class)
{
    GObjectClass *g_object_class = G_OBJECT_CLASS(class);
    g_object_class->set_property = plinia_zoom_level_chooser_set_property;
    g_object_class->get_property = plinia_zoom_level_chooser_get_property;

    signals[SIG_ACTIVATE] = g_signal_new("activate", PLINIA_TYPE_ZOOM_LEVEL_CHOOSER, G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL, G_TYPE_NONE, 1,
        G_TYPE_DOUBLE // Zoom level.
        );

    properties[PROP_MIN_VALUE] = g_param_spec_uint("min-value", _("Minimum zoom level"), _("Minimum allowed zoom level value"), 1, G_MAXUINT, 1, G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
    properties[PROP_MAX_VALUE] = g_param_spec_uint("max-value", _("Maximum zoom level"), _("Maximum allowed zoom level value"), 1, G_MAXUINT, G_MAXUINT, G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
    properties[PROP_DEF_VALUE] = g_param_spec_uint("def-value", _("Default zoom level"), _("Default zoom level value"), 1, G_MAXUINT, 1, G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

    g_object_class_install_properties(g_object_class, N_PROPERTIES, properties);
}

static void plinia_zoom_level_chooser_init(PliniaZoomLevelChooser *instance)
{
    PliniaZoomLevelChooserPrivate *private = plinia_zoom_level_chooser_get_instance_private(instance);

    /* INITIALING PROPERTIES */

    instance->min_value = 1;
    instance->max_value = G_MAXUINT;
    instance->def_value = 1;

    /* GUI CREATION */

    // Create element widgets and set their properties.
    private->gui.current_zoom_level = GTK_ENTRY(gtk_entry_new());
    gtk_widget_add_events(GTK_WIDGET(private->gui.current_zoom_level), GDK_SCROLL_MASK);
    gtk_entry_set_input_purpose(private->gui.current_zoom_level, GTK_INPUT_PURPOSE_DIGITS);
    gtk_entry_set_input_hints(private->gui.current_zoom_level, GTK_INPUT_HINT_NO_EMOJI);
    PLINIA_ZOOM_LEVEL_CHOOSER_SET_CURRENT_ZOOM_LEVEL_ENTRY_WIDTH(instance, private)
    private->gui.percentage_entry = GTK_ENTRY(gtk_entry_new());
    gtk_widget_set_sensitive(GTK_WIDGET(private->gui.percentage_entry), FALSE);
    gtk_entry_set_width_chars(private->gui.percentage_entry, 1);
    gtk_entry_set_text(private->gui.percentage_entry, "%");

    // Create content area and set its properties.
    private->gui.content_area = GTK_BOX(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0));
    GtkStyleContext *content_area_style_context = gtk_widget_get_style_context(GTK_WIDGET(private->gui.content_area));
    gtk_style_context_add_class(content_area_style_context, GTK_STYLE_CLASS_RAISED);
    gtk_style_context_add_class(content_area_style_context, GTK_STYLE_CLASS_LINKED);

    // Attach element widgets to the content area.
    gtk_box_pack_start(private->gui.content_area, GTK_WIDGET(private->gui.current_zoom_level), FALSE, FALSE, 0);
    gtk_box_pack_start(private->gui.content_area, GTK_WIDGET(private->gui.percentage_entry), FALSE, FALSE, 0);

    // Add content area to the zoom level chooser widget itself.
    gtk_container_add(GTK_CONTAINER(instance), GTK_WIDGET(private->gui.content_area));
}

/* API */

GtkToolItem *plinia_zoom_level_chooser_new(guint min_value, guint max_value, guint def_value)
{
    g_return_val_if_fail(min_value > 0, NULL);
    g_return_val_if_fail(max_value >= min_value, NULL);
    g_return_val_if_fail(def_value >= min_value && def_value <= max_value, NULL);

    return g_object_new(PLINIA_TYPE_ZOOM_LEVEL_CHOOSER, "min-value", min_value, "max-value", max_value, "def-value", def_value, NULL);
}

guint plinia_zoom_level_chooser_get_min_value(PliniaZoomLevelChooser *zoom_level_chooser)
{
    g_return_val_if_fail(PLINIA_IS_ZOOM_LEVEL_CHOOSER(zoom_level_chooser), 0);

    guint min_value;
    g_object_get(zoom_level_chooser, "min-value", &min_value, NULL);

    return min_value;
}

guint plinia_zoom_level_chooser_get_max_value(PliniaZoomLevelChooser *zoom_level_chooser)
{
    g_return_val_if_fail(PLINIA_IS_ZOOM_LEVEL_CHOOSER(zoom_level_chooser), 0);

    guint max_value;
    g_object_get(zoom_level_chooser, "max-value", &max_value, NULL);

    return max_value;
}

guint plinia_zoom_level_chooser_get_def_value(PliniaZoomLevelChooser *zoom_level_chooser)
{
    g_return_val_if_fail(PLINIA_IS_ZOOM_LEVEL_CHOOSER(zoom_level_chooser), 0);

    guint def_value;
    g_object_get(zoom_level_chooser, "def-value", &def_value, NULL);

    return def_value;
}
