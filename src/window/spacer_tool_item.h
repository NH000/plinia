#ifndef PLINIA_SPACER_TOOL_ITEM_H
#define PLINIA_SPACER_TOOL_ITEM_H
#include <gtk/gtk.h>
#include "separator_invisible_tool_item.h"

#define PLINIA_TYPE_SPACER_TOOL_ITEM    plinia_spacer_tool_item_get_type()
G_DECLARE_FINAL_TYPE(PliniaSpacerToolItem, plinia_spacer_tool_item, PLINIA, SPACER_TOOL_ITEM, PliniaSeparatorInvisibleToolItem)

GtkToolItem *plinia_spacer_tool_item_new();

#endif
