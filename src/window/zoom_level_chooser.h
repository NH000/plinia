#ifndef PLINIA_ZOOM_LEVEL_CHOOSER_H
#define PLINIA_ZOOM_LEVEL_CHOOSER_H
#include <gtk/gtk.h>

#define PLINIA_TYPE_ZOOM_LEVEL_CHOOSER   plinia_zoom_level_chooser_get_type()
G_DECLARE_FINAL_TYPE(PliniaZoomLevelChooser, plinia_zoom_level_chooser, PLINIA, ZOOM_LEVEL_CHOOSER, GtkToolItem)

GtkToolItem *plinia_zoom_level_chooser_new(guint min_value, guint max_value, guint def_value);
guint plinia_zoom_level_chooser_get_max_value(PliniaZoomLevelChooser *zoom_level_chooser);
guint plinia_zoom_level_chooser_get_min_value(PliniaZoomLevelChooser *zoom_level_chooser);
guint plinia_zoom_level_chooser_get_def_value(PliniaZoomLevelChooser *zoom_level_chooser);

#endif
