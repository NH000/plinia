#include <glib/gi18n.h>
#include "window.h"
#include "../info.h"
#include "../auxillary.h"
#include "../view.h"

typedef struct
{
    GtkGrid *content_area;  // Content area of the window.
    PliniaMenubar *menubar; // Menu bar of the window.
    PliniaToolbar *toolbar; // Tool bar of the window.
} PliniaWindowGUI;

typedef struct
{
    GdkPixbuf *icon;        // Window icon.
    PliniaWindowGUI gui;    // Window widgets.
} PliniaWindowPrivate;

struct _PliniaWindow
{
    GtkApplicationWindow parent_instance;
};

G_DEFINE_TYPE_WITH_PRIVATE(PliniaWindow, plinia_window, GTK_TYPE_APPLICATION_WINDOW)

static void plinia_window_finalize(GObject *g_object)
{
    PliniaWindow *instance = PLINIA_WINDOW(g_object);
    PliniaWindowPrivate *private = plinia_window_get_instance_private(instance);

    if (private->icon)
        g_object_unref(private->icon);

    G_OBJECT_CLASS(plinia_window_parent_class)->finalize(g_object);
}

static void plinia_window_class_init(PliniaWindowClass *class)
{
    GObjectClass *g_object_class = G_OBJECT_CLASS(class);
    g_object_class->finalize = plinia_window_finalize;
}

static void plinia_window_init(PliniaWindow *instance)
{
    PliniaWindowPrivate *private = plinia_window_get_instance_private(instance);

    /* WINDOW PROPERTIES */

    GtkWindow *gtk_window = GTK_WINDOW(instance);

    gtk_window_set_title(gtk_window, PLINIA_INFO_NAME);
    gtk_window_set_default_size(gtk_window, 640, 720);

    // Set window icon.
    if ((private->icon = gtk_icon_theme_load_icon(gtk_icon_theme_get_default(), PLINIA_EXEC_NAME, 48, GTK_ICON_LOOKUP_FORCE_SIZE, NULL)))
        gtk_window_set_icon(gtk_window, private->icon);

    /* GUI CREATION */

    // Create window menubar.
    private->gui.menubar = PLINIA_MENUBAR(plinia_menubar_new());
    gtk_widget_set_hexpand(GTK_WIDGET(private->gui.menubar), TRUE);

    // Create window tool bar.
    private->gui.toolbar = PLINIA_TOOLBAR(plinia_toolbar_new());
    gtk_widget_set_hexpand(GTK_WIDGET(private->gui.toolbar), TRUE);

    // Create content area and attach window widgets to it.
    private->gui.content_area = GTK_GRID(gtk_grid_new());
    gtk_grid_attach(private->gui.content_area, GTK_WIDGET(private->gui.menubar), 0, 0, 1, 1);
    gtk_grid_attach(private->gui.content_area, GTK_WIDGET(private->gui.toolbar), 0, 1, 1, 1);

    // Add content area to the window.
    gtk_container_add(GTK_CONTAINER(instance), GTK_WIDGET(private->gui.content_area));

    // Make window contents visible by default, everything except for the toolbar.
    gtk_widget_show(GTK_WIDGET(private->gui.menubar));
    gtk_widget_show(GTK_WIDGET(private->gui.content_area));

    /* ADD ACTIONS */

    GActionMap *g_action_map = G_ACTION_MAP(instance);

    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("file.open", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("file.open-a-copy", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("file.save-a-copy", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("file.reload", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("file.properties", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("file.close", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("edit.find", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("edit.find-next", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("edit.find-prev", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("edit.rotate-left", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("edit.rotate-right", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("edit.copy", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("edit.select-all", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("edit.add-bookmark", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("edit.save-current-settings-as-default", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new_stateful("view.show-toolbar", NULL, g_variant_new_boolean(FALSE))));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new_stateful("view.show-sidebar", NULL, g_variant_new_boolean(FALSE))));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new_stateful("view.continuous", NULL, g_variant_new_boolean(FALSE))));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new_stateful("view.dual", NULL, g_variant_new_byte(PLINIA_VIEW_DUAL_MODE_OFF))));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new_stateful("view.dual.direction", NULL, g_variant_new_byte(PLINIA_VIEW_DUAL_DIRECTION_LEFT_TO_RIGHT))));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new_stateful("view.inverted-colors", NULL, g_variant_new_boolean(FALSE))));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("view.zoom-in", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("view.zoom-out", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("view.zoom-reset", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("view.fit-width", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("view.fit-height", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("view.resize-window-to-fit", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new_stateful("view.fullscreen", NULL, g_variant_new_boolean(FALSE))));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new_stateful("view.presentation", NULL, g_variant_new_boolean(FALSE))));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("go.next-page", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("go.prev-page", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("go.first-page", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("go.last-page", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("go.jump-to-page", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("history.backward", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("history.forward", NULL)));
    g_action_map_add_action(g_action_map, G_ACTION(g_simple_action_new("help.about", NULL)));

    /* CONNECT ACTIONS TO THEIR SIGNAL HANDLERS */

    g_signal_connect(g_action_map_lookup_action(g_action_map, "view.show-toolbar"), "change-state", G_CALLBACK(plinia_view_show_toolbar), instance);
}

/* API */

GtkWidget *plinia_window_new(PliniaApplication *application)
{
    g_return_val_if_fail(PLINIA_IS_APPLICATION(application), NULL);

    return g_object_new(PLINIA_TYPE_WINDOW, "application", application, NULL);
}

GtkWidget *plinia_window_get_menubar(PliniaWindow *window)
{
    g_return_val_if_fail(PLINIA_IS_WINDOW(window), NULL);

    PliniaWindowPrivate *private = plinia_window_get_instance_private(window);

    return GTK_WIDGET(private->gui.menubar);
}

GtkWidget *plinia_window_get_toolbar(PliniaWindow *window)
{
    g_return_val_if_fail(PLINIA_IS_WINDOW(window), NULL);

    PliniaWindowPrivate *private = plinia_window_get_instance_private(window);

    return GTK_WIDGET(private->gui.toolbar);
}

void plinia_window_add_accelerators(PliniaWindow *window)
{
    g_return_if_fail(PLINIA_IS_WINDOW(window));

// Adds supplied accelerator to the application.
#define PLINIA_WINDOW_ADD_ACCELERATOR(application, action, ...) {                                                                       \
                                                                    const char *accels[] = {__VA_ARGS__, NULL};                         \
                                                                    gtk_application_set_accels_for_action(application, action, accels); \
                                                                }
#define PLINIA_WINDOW_ADD_ACCELERATOR_NUM(application, action, num, ...)    {                                                                           \
                                                                                char *action_num = g_strdup_printf(action "(%d)", num);                 \
                                                                                PLINIA_WINDOW_ADD_ACCELERATOR(application, action_num, __VA_ARGS__);    \
                                                                                g_free(action_num);                                                     \
                                                                            }

    GtkApplication *gtk_application = gtk_window_get_application(GTK_WINDOW(window));

    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.file.open", "<Ctrl>O")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.file.open-a-copy", "<Ctrl><Shift>O")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.file.save-a-copy", "<Ctrl><Shift>S")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.file.reload", "<Ctrl>R")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.file.close", "<Ctrl>Q")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.edit.find", "<Ctrl>F")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.edit.find-prev", "<Ctrl>G")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.edit.find-next", "<Ctrl><Shift>G")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.edit.rotate-left", "<Ctrl>Left")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.edit.rotate-right", "<Ctrl>Right")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.edit.copy", "<Ctrl>C")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.edit.select-all", "<Ctrl>A")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.edit.add-bookmark", "<Ctrl>B")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.view.show-toolbar", "<Ctrl><Shift>T")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.view.show-sidebar", "F9")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.view.continuous", "<Ctrl><Shift>C")
    PLINIA_WINDOW_ADD_ACCELERATOR_NUM(gtk_application, "win.view.dual", PLINIA_VIEW_DUAL_MODE_OFF, "<Shift>D")
    PLINIA_WINDOW_ADD_ACCELERATOR_NUM(gtk_application, "win.view.dual.direction", PLINIA_VIEW_DUAL_DIRECTION_LEFT_TO_RIGHT, "D")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.view.inverted-colors", "<Ctrl>I")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.view.zoom-in", "<Ctrl>plus")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.view.zoom-out", "<Ctrl>minus")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.view.zoom-reset", "<Ctrl>0")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.view.fit-width", "W")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.view.fit-height", "H")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.view.resize-window-to-fit", "<Ctrl><Shift>R")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.view.fullscreen", "F11")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.view.presentation", "F5")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.go.next-page", "Page_Down")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.go.prev-page", "Page_Up")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.go.first-page", "Home")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.go.last-page", "End")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.go.jump-to-page", "<Ctrl>J")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.history.backward", "BackSpace")
    PLINIA_WINDOW_ADD_ACCELERATOR(gtk_application, "win.history.forward", "<Shift>BackSpace")

#undef PLINIA_WINDOW_ADD_ACCELERATOR_NUM
#undef PLINIA_WINDOW_ADD_ACCELERATOR
}

void plinia_window_set_file_enabled(PliniaWindow *window, gboolean enabled)
{
    g_return_if_fail(PLINIA_IS_WINDOW(window));

    GActionMap *g_action_map = G_ACTION_MAP(window);

    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "file.open-a-copy")), enabled);
    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "file.save-a-copy")), enabled);
    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "file.reload")), enabled);
    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "file.properties")), enabled);
    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "edit.find")), enabled);
    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "edit.rotate-left")), enabled);
    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "edit.rotate-right")), enabled);
    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "edit.select-all")), enabled);
    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "view.zoom-in")), enabled);
    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "view.zoom-out")), enabled);
    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "view.zoom-reset")), enabled);
    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "view.fit-width")), enabled);
    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "view.fit-height")), enabled);
    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "view.resize-window-to-fit")), enabled);
    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "view.presentation")), enabled);
    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "go.first-page")), enabled);
    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "go.last-page")), enabled);
    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "go.jump-to-page")), enabled);
}

void plinia_window_set_find_prev_and_next_enabled(PliniaWindow *window, gboolean enabled)
{
    g_return_if_fail(PLINIA_IS_WINDOW(window));

    GActionMap *g_action_map = G_ACTION_MAP(window);

    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "edit.find-prev")), enabled);
    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "edit.find-next")), enabled);
}

void plinia_window_set_copy_enabled(PliniaWindow *window, gboolean enabled)
{
    g_return_if_fail(PLINIA_IS_WINDOW(window));

    GActionMap *g_action_map = G_ACTION_MAP(window);

    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "edit.copy")), enabled);
}

void plinia_window_set_bookmark_enabled(PliniaWindow *window, gboolean enabled)
{
    g_return_if_fail(PLINIA_IS_WINDOW(window));

    GActionMap *g_action_map = G_ACTION_MAP(window);

    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "edit.add-bookmark")), enabled);
}

void plinia_window_set_navigation_enabled(PliniaWindow *window, gboolean first, gboolean last)
{
    g_return_if_fail(PLINIA_IS_WINDOW(window));

    GActionMap *g_action_map = G_ACTION_MAP(window);

    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "go.prev-page")), !first);
    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "go.first-page")), !first);
    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "go.next-page")), !last);
    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "go.last-page")), !last);
}

void plinia_window_set_history_enabled(PliniaWindow *window, gboolean bottom, gboolean top)
{
    g_return_if_fail(PLINIA_IS_WINDOW(window));

    GActionMap *g_action_map = G_ACTION_MAP(window);

    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "history.backward")), !bottom);
    g_simple_action_set_enabled(G_SIMPLE_ACTION(g_action_map_lookup_action(g_action_map, "history.forward")), !top);
}

void plinia_window_configure_from_options(PliniaWindow *window, const PliniaApplicationOptions *application_options)
{
    g_return_if_fail(PLINIA_IS_WINDOW(window));
    g_return_if_fail(application_options != NULL);

    GActionMap *g_action_map = G_ACTION_MAP(window);

    g_action_change_state(g_action_map_lookup_action(g_action_map, "view.show-toolbar"), g_variant_new_boolean(application_options->view.show_toolbar));
    g_action_change_state(g_action_map_lookup_action(g_action_map, "view.show-sidebar"), g_variant_new_boolean(application_options->view.show_sidebar));
    g_action_change_state(g_action_map_lookup_action(g_action_map, "view.continuous"), g_variant_new_boolean(application_options->view.continuous));
    g_action_change_state(g_action_map_lookup_action(g_action_map, "view.dual"), g_variant_new_byte(application_options->view.dual));
    g_action_change_state(g_action_map_lookup_action(g_action_map, "view.dual.direction"), g_variant_new_byte(application_options->view.dual_direction));
    g_action_change_state(g_action_map_lookup_action(g_action_map, "view.inverted-colors"), g_variant_new_boolean(application_options->view.inverted_colors));
}
