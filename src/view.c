#include <string.h>
#include "view.h"
#include "application.h"
#include "document/document.h"
#include "window/toolbar.h"

/* DUAL PREVIEW MODE */

GType plinia_view_dual_mode_get_type()
{
    static GType type = 0;

    if (type == 0)
    {
        static const GEnumValue values[] =  {
                                                {PLINIA_VIEW_DUAL_MODE_OFF, "PLINIA_VIEW_DUAL_MODE_OFF", "off"},
                                                {PLINIA_VIEW_DUAL_MODE_EVEN_PAGES_LEFT, "PLINIA_VIEW_DUAL_MODE_EVEN_PAGES_LEFT", "even-pages-left"},
                                                {PLINIA_VIEW_DUAL_MODE_EVEN_PAGES_RIGHT, "PLINIA_VIEW_DUAL_MODE_EVEN_PAGES_RIGHT", "even-pages-right"},
                                                {0, NULL, NULL}
                                            };

        type = g_enum_register_static("PliniaViewDualMode", values);
    }

    return type;
}

/* DUAL PREVIEW DIRECTION MODE */

GType plinia_view_dual_direction_get_type()
{
    static GType type = 0;

    if (type == 0)
    {
        static const GEnumValue values[] =  {
                                                {PLINIA_VIEW_DUAL_DIRECTION_LEFT_TO_RIGHT, "PLINIA_VIEW_DUAL_DIRECTION_LEFT_TO_RIGHT", "left-to-right"},
                                                {PLINIA_VIEW_DUAL_DIRECTION_RIGHT_TO_LEFT, "PLINIA_VIEW_DUAL_DIRECTION_RIGHT_TO_LEFT", "right-to-left"},
                                                {0, NULL, NULL}
                                            };

        type = g_enum_register_static("PliniaViewDualDirection", values);
    }

    return type;
}

/* VIEW */

void plinia_view_show_toolbar(GSimpleAction *action, GVariant *state, PliniaWindow *window)
{
    g_return_if_fail(G_IS_SIMPLE_ACTION(action));
    g_return_if_fail(state != NULL);
    g_return_if_fail(PLINIA_IS_WINDOW(window));

    PliniaApplication *application = PLINIA_APPLICATION(gtk_window_get_application(GTK_WINDOW(window)));

    PliniaApplicationOptions application_options;
    memcpy(&application_options, plinia_application_options_get(application), sizeof(PliniaApplicationOptions));

    application_options.view.show_toolbar = g_variant_get_boolean(state);

    PliniaDocument *document = plinia_application_get_document(application);

    if (document)
    {
        if (application_options.view.show_toolbar)
            gtk_widget_show_all(plinia_window_get_toolbar(window));
        else
            gtk_widget_hide(plinia_window_get_toolbar(window));

        g_object_unref(document);
    }

    g_simple_action_set_state(action, state);

    plinia_application_options_set(application, &application_options);
}
