#include <glib/gi18n.h>
#include "enter_password.h"
#include "../auxillary.h"

typedef struct
{
    GtkGrid *password_area;     // Area of the dialog where password message and entry is.
    GtkImage *icon;             // Dialog icon.
    GtkLabel *message;          // Message dispayed to the user.
    GtkLabel *password_label;   // Label that marks password entry.
    GtkEntry *password_entry;   // Entry that allows user to enter password.
} PliniaDialogEnterPasswordGUI;

typedef struct
{
    PliniaDialogEnterPasswordGUI gui;   // Window widgets.
} PliniaDialogEnterPasswordPrivate;

struct _PliniaDialogEnterPassword
{
    PliniaDialog parent_instance;

    char *message;  // Message displayed to the user.
};

G_DEFINE_TYPE_WITH_PRIVATE(PliniaDialogEnterPassword, plinia_dialog_enter_password, PLINIA_TYPE_DIALOG)

typedef enum
{
    PROP_MESSAGE = 1,
    N_PROPERTIES
} PliniaDialogEnterPasswordProperty;

static GParamSpec *properties[N_PROPERTIES] = {NULL};

static void plinia_dialog_enter_password_set_property(GObject *g_object, guint property_id, const GValue *value, GParamSpec *pspec)
{
    PliniaDialogEnterPassword *instance = PLINIA_DIALOG_ENTER_PASSWORD(g_object);
    PliniaDialogEnterPasswordPrivate *private = plinia_dialog_enter_password_get_instance_private(instance);

    switch (property_id)
    {
        case PROP_MESSAGE:
            g_free(instance->message);
            instance->message = g_value_dup_string(value);

            // Update GUI message widget text.
            gtk_label_set_text(private->gui.message, instance->message);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(g_object, property_id, pspec);
    }
}

static void plinia_dialog_enter_password_get_property(GObject *g_object, guint property_id, GValue *value, GParamSpec *pspec)
{
    PliniaDialogEnterPassword *instance = PLINIA_DIALOG_ENTER_PASSWORD(g_object);

    switch (property_id)
    {
        case PROP_MESSAGE:
            g_value_set_string(value, instance->message);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(g_object, property_id, pspec);
    }
}

static void plinia_dialog_enter_password_class_init(PliniaDialogEnterPasswordClass *class)
{
    GObjectClass *g_object_class = G_OBJECT_CLASS(class);
    g_object_class->set_property = plinia_dialog_enter_password_set_property;
    g_object_class->get_property = plinia_dialog_enter_password_get_property;

    properties[PROP_MESSAGE] = g_param_spec_string("message", _("Message"), _("Message displayed to the user."), NULL, G_PARAM_READWRITE | G_PARAM_CONSTRUCT);

    g_object_class_install_properties(g_object_class, N_PROPERTIES, properties);
}

static void plinia_dialog_enter_password_init(PliniaDialogEnterPassword *instance)
{
    PliniaDialogEnterPasswordPrivate *private = plinia_dialog_enter_password_get_instance_private(instance);

    /* GUI CREATION */

    GtkDialog *gtk_dialog = GTK_DIALOG(instance);

    // Add dialog buttons.
    gtk_dialog_add_button(gtk_dialog, _("Cancel"), GTK_RESPONSE_CANCEL);
    gtk_dialog_add_button(gtk_dialog, _("Unlock"), GTK_RESPONSE_ACCEPT);

    // Let "Unlock" button be the default response.
    gtk_dialog_set_default_response(gtk_dialog, GTK_RESPONSE_ACCEPT);

    // Create dialog icon.
    private->gui.icon = GTK_IMAGE(gtk_image_new_from_icon_name("dialog-password", GTK_ICON_SIZE_DIALOG));

    // Create dialog message.
    private->gui.message = GTK_LABEL(gtk_label_new(NULL));
    gtk_widget_set_halign(GTK_WIDGET(private->gui.message), GTK_ALIGN_START);

    // Create password mark label.
    private->gui.password_label = GTK_LABEL(gtk_label_new(_("Password:")));
    gtk_widget_set_halign(GTK_WIDGET(private->gui.password_label), GTK_ALIGN_START);

    // Create password entry.
    private->gui.password_entry = GTK_ENTRY(gtk_entry_new());
    gtk_widget_set_hexpand(GTK_WIDGET(private->gui.password_entry), TRUE);
    gtk_entry_set_visibility(private->gui.password_entry, FALSE);
    gtk_entry_set_input_purpose(private->gui.password_entry, GTK_INPUT_PURPOSE_PASSWORD);
    gtk_entry_set_input_hints(private->gui.password_entry, GTK_INPUT_HINT_NO_EMOJI);

    {
        GtkWidget *button_unlock = gtk_dialog_get_widget_for_response(gtk_dialog, GTK_RESPONSE_ACCEPT);

        // Disable "Unlock" button when password entry is empty.
        gtk_widget_set_sensitive(button_unlock, FALSE);
        g_signal_connect(private->gui.password_entry, "changed", G_CALLBACK(plinia_auxillary_set_widget_state_on_entry_changed), button_unlock);

        // Click "Unlock" button when user activates password entry.
        g_signal_connect_swapped(private->gui.password_entry, "activate", G_CALLBACK(plinia_auxillary_click_button_if_sensitive), button_unlock);
    }

    // Create password content area.
    private->gui.password_area = GTK_GRID(gtk_grid_new());
    gtk_grid_set_row_spacing(private->gui.password_area, 5);
    gtk_grid_set_column_spacing(private->gui.password_area, 5);

    // Add password widgets to the password content area.
    gtk_grid_attach(private->gui.password_area, GTK_WIDGET(private->gui.icon), 0, 0, 1, 2);
    gtk_grid_attach(private->gui.password_area, GTK_WIDGET(private->gui.message), 1, 0, 2, 1);
    gtk_grid_attach(private->gui.password_area, GTK_WIDGET(private->gui.password_label), 1, 1, 1, 1);
    gtk_grid_attach(private->gui.password_area, GTK_WIDGET(private->gui.password_entry), 2, 1, 1, 1);

    // Add password area to dialog area.
    gtk_box_pack_start(GTK_BOX(gtk_dialog_get_content_area(gtk_dialog)), GTK_WIDGET(private->gui.password_area), TRUE, TRUE, 0);

    // Display password area.
    gtk_widget_show_all(GTK_WIDGET(private->gui.password_area));
}

/* API */

GtkWidget *plinia_dialog_enter_password_new(GtkWindow *parent, const char *title, const char *message)
{
    g_return_val_if_fail(!parent || GTK_IS_WINDOW(parent), NULL);

    return g_object_new(PLINIA_TYPE_DIALOG_ENTER_PASSWORD, "transient-for", parent, "title", title, "message", message, NULL);
}

// Runs the dialog and returns the password that user entered, or NULL if the operation was cancelled.
const char *plinia_dialog_enter_password_run(PliniaDialogEnterPassword *dialog_enter_password)
{
    g_return_val_if_fail(PLINIA_IS_DIALOG_ENTER_PASSWORD(dialog_enter_password), NULL);

    PliniaDialogEnterPasswordPrivate *private = plinia_dialog_enter_password_get_instance_private(dialog_enter_password);

    if (gtk_dialog_run(GTK_DIALOG(dialog_enter_password)) == GTK_RESPONSE_ACCEPT)
        return gtk_entry_get_text(private->gui.password_entry);

    return NULL;
}
