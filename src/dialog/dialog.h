#ifndef PLINIA_DIALOG_H
#define PLINIA_DIALOG_H
#include <gtk/gtk.h>

#define PLINIA_TYPE_DIALOG  plinia_dialog_get_type()
G_DECLARE_DERIVABLE_TYPE(PliniaDialog, plinia_dialog, PLINIA, DIALOG, GtkDialog)

struct _PliniaDialogClass
{
    GtkDialogClass parent_class;
};

GtkWidget *plinia_dialog_new();

#endif
