#include "dialog.h"

G_DEFINE_TYPE(PliniaDialog, plinia_dialog, GTK_TYPE_DIALOG)

static void plinia_dialog_class_init(PliniaDialogClass *class)
{
}

static void plinia_dialog_init(PliniaDialog *instance)
{
    g_object_set(instance, "border-width", 5, "modal", TRUE, "resizable", FALSE, NULL);
    g_object_set(gtk_dialog_get_content_area(GTK_DIALOG(instance)), "spacing", 5, NULL);
}

/* API */

GtkWidget *plinia_dialog_new()
{
    return g_object_new(PLINIA_TYPE_DIALOG, NULL);
}
