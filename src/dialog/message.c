#include <glib/gi18n.h>
#include "message.h"

typedef struct
{
    GtkBox *message_area;   // Area that holds message label and icon.
    GtkImage *icon;         // Icon.
    GtkLabel *message;      // Message.
} PliniaDialogMessageGUI;

typedef struct
{
    PliniaDialogMessageGUI gui; // Window widgets.
} PliniaDialogMessagePrivate;

struct _PliniaDialogMessage
{
    PliniaDialog parent_instance;

    char *message;                  // Message displayed to the user.
    GtkMessageType message_type;    // Type of the message. Icon is set from this property.
    GtkButtonsType buttons_type;    // Buttons of the dialog.
};

G_DEFINE_TYPE_WITH_PRIVATE(PliniaDialogMessage, plinia_dialog_message, PLINIA_TYPE_DIALOG)

typedef enum
{
    PROP_MESSAGE = 1,
    PROP_MESSAGE_TYPE,
    PROP_BUTTONS_TYPE,
    N_PROPERTIES
} PliniaDialogMessageProperty;

static GParamSpec *properties[N_PROPERTIES] = {NULL};

static void plinia_dialog_message_set_property(GObject *g_object, guint property_id, const GValue *value, GParamSpec *pspec)
{
    PliniaDialogMessage *instance = PLINIA_DIALOG_MESSAGE(g_object);
    PliniaDialogMessagePrivate *private = plinia_dialog_message_get_instance_private(instance);

    switch (property_id)
    {
        case PROP_MESSAGE:
            g_free(instance->message);
            instance->message = g_value_dup_string(value);

            // Update message label widget.
            gtk_label_set_text(private->gui.message, instance->message);
            break;
        case PROP_MESSAGE_TYPE:
            instance->message_type = g_value_get_int(value);
            break;
        case PROP_BUTTONS_TYPE:
            instance->buttons_type = g_value_get_int(value);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(g_object, property_id, pspec);
    }
}

static void plinia_dialog_message_get_property(GObject *g_object, guint property_id, GValue *value, GParamSpec *pspec)
{
    PliniaDialogMessage *instance = PLINIA_DIALOG_MESSAGE(g_object);

    switch (property_id)
    {
        case PROP_MESSAGE:
            g_value_set_string(value, instance->message);
            break;
        case PROP_MESSAGE_TYPE:
            g_value_set_int(value, instance->message_type);
            break;
        case PROP_BUTTONS_TYPE:
            g_value_set_int(value, instance->buttons_type);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(g_object, property_id, pspec);
    }
}

static void plinia_dialog_message_constructed(GObject *g_object)
{
    PliniaDialogMessage *instance = PLINIA_DIALOG_MESSAGE(g_object);
    PliniaDialogMessagePrivate *private = plinia_dialog_message_get_instance_private(instance);

    // Set dialog icon.
    gtk_image_set_from_icon_name(private->gui.icon, instance->message_type == GTK_MESSAGE_INFO ? "dialog-information"
                                                    : (instance->message_type == GTK_MESSAGE_QUESTION ? "dialog-question"
                                                    : (instance->message_type == GTK_MESSAGE_WARNING ? "dialog-warning"
                                                    : (instance->message_type == GTK_MESSAGE_ERROR ? "dialog-error" : NULL))),
                                                    GTK_ICON_SIZE_DIALOG);

    // Add dialog buttons.
    switch (instance->buttons_type)
    {
        case GTK_BUTTONS_OK:
            gtk_dialog_add_button(GTK_DIALOG(g_object), _("OK"), GTK_RESPONSE_OK);
            break;
        case GTK_BUTTONS_CLOSE:
            gtk_dialog_add_button(GTK_DIALOG(g_object), _("Close"), GTK_RESPONSE_CLOSE);
            break;
        case GTK_BUTTONS_CANCEL:
            gtk_dialog_add_button(GTK_DIALOG(g_object), _("Cancel"), GTK_RESPONSE_CANCEL);
            break;
        case GTK_BUTTONS_YES_NO:
            gtk_dialog_add_button(GTK_DIALOG(g_object), _("Yes"), GTK_RESPONSE_YES);
            gtk_dialog_add_button(GTK_DIALOG(g_object), _("No"), GTK_RESPONSE_NO);
            break;
        case GTK_BUTTONS_OK_CANCEL:
            gtk_dialog_add_button(GTK_DIALOG(g_object), _("OK"), GTK_RESPONSE_OK);
            gtk_dialog_add_button(GTK_DIALOG(g_object), _("Cancel"), GTK_RESPONSE_CANCEL);
            break;
        default:
            break;
    }

    G_OBJECT_CLASS(plinia_dialog_message_parent_class)->constructed(g_object);
}

static void plinia_dialog_message_finalize(GObject *g_object)
{
    PliniaDialogMessage *instance = PLINIA_DIALOG_MESSAGE(g_object);

    g_free(instance->message);

    G_OBJECT_CLASS(plinia_dialog_message_parent_class)->finalize(g_object);
}

static void plinia_dialog_message_class_init(PliniaDialogMessageClass *class)
{
    GObjectClass *g_object_class = G_OBJECT_CLASS(class);
    g_object_class->set_property = plinia_dialog_message_set_property;
    g_object_class->get_property = plinia_dialog_message_get_property;
    g_object_class->constructed = plinia_dialog_message_constructed;
    g_object_class->finalize = plinia_dialog_message_finalize;

    properties[PROP_MESSAGE] = g_param_spec_string("message", _("Message"), _("Message displayed to the user"), NULL, G_PARAM_READWRITE | G_PARAM_CONSTRUCT);
    properties[PROP_MESSAGE_TYPE] = g_param_spec_int("message-type", _("Message type"), _("Type of the message (determines which icon is displayed)"), G_MININT, G_MAXINT, GTK_MESSAGE_OTHER, G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
    properties[PROP_BUTTONS_TYPE] = g_param_spec_int("buttons-type", _("Buttons type"), _("Buttons of the dialog"), G_MININT, G_MAXINT, GTK_BUTTONS_NONE, G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

    g_object_class_install_properties(g_object_class, N_PROPERTIES, properties);
}

static void plinia_dialog_message_init(PliniaDialogMessage *instance)
{
    PliniaDialogMessagePrivate *private = plinia_dialog_message_get_instance_private(instance);

    /* GUI CREATION */

    GtkDialog *gtk_dialog = GTK_DIALOG(instance);

    // Create and add dialog icon.
    private->gui.icon = GTK_IMAGE(gtk_image_new());

    // Create dialog message.
    private->gui.message = GTK_LABEL(gtk_label_new(NULL));

    // Create message area and add message elements to it.
    private->gui.message_area = GTK_BOX(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5));
    gtk_container_set_border_width(GTK_CONTAINER(private->gui.message_area), 5);
    if (private->gui.icon)
        gtk_box_pack_start(private->gui.message_area, GTK_WIDGET(private->gui.icon), FALSE, FALSE, 0);
    gtk_box_pack_start(private->gui.message_area, GTK_WIDGET(private->gui.message), FALSE, FALSE, 0);

    // Add message area to the dialog and show it.
    GtkBox *content_area = GTK_BOX(gtk_dialog_get_content_area(gtk_dialog));
    gtk_box_pack_start(content_area, GTK_WIDGET(private->gui.message_area), TRUE, TRUE, 0);
    gtk_widget_show_all(GTK_WIDGET(private->gui.message_area));

    // Spread out the buttons.
    gtk_container_set_border_width(GTK_CONTAINER(content_area), 0);
    gtk_container_set_border_width(GTK_CONTAINER(gtk_dialog), 0);
    gtk_button_box_set_layout(GTK_BUTTON_BOX(gtk_dialog_get_action_area(gtk_dialog)), GTK_BUTTONBOX_EXPAND);

    // Destroy dialog when user responds.
    g_signal_connect(gtk_dialog, "response", G_CALLBACK(gtk_widget_destroy), NULL);
}

/* API */

GtkWidget *plinia_dialog_message_new(GtkWindow *parent, const char *title, const char *message, GtkMessageType message_type, GtkButtonsType buttons_type)
{
    g_return_val_if_fail(!parent || GTK_IS_WINDOW(parent), NULL);

    return g_object_new(PLINIA_TYPE_DIALOG_MESSAGE, "transient-for", parent, "title", title, "message", message, "message-type", message_type, "buttons-type", buttons_type, NULL);
}

void plinia_dialog_message_set_message(PliniaDialogMessage *dialog_message, const char *message)
{
    g_return_if_fail(PLINIA_IS_DIALOG_MESSAGE(dialog_message));

    g_object_set(dialog_message, "message", message, NULL);
}

const char *plinia_dialog_message_get_message(PliniaDialogMessage *dialog_message)
{
    g_return_val_if_fail(PLINIA_IS_DIALOG_MESSAGE(dialog_message), NULL);

    const char *message;
    g_object_get(dialog_message, "message", &message, NULL);

    return message;
}

GtkMessageType plinia_dialog_message_get_message_type(PliniaDialogMessage *dialog_message)
{
    g_return_val_if_fail(PLINIA_IS_DIALOG_MESSAGE(dialog_message), GTK_MESSAGE_OTHER);

    GtkMessageType message_type;
    g_object_get(dialog_message, "message-type", &message_type, NULL);

    return message_type;
}

GtkButtonsType plinia_dialog_message_get_buttons_type(PliniaDialogMessage *dialog_message)
{
    g_return_val_if_fail(PLINIA_IS_DIALOG_MESSAGE(dialog_message), GTK_BUTTONS_NONE);

    GtkButtonsType buttons_type;
    g_object_get(dialog_message, "buttons-type", &buttons_type, NULL);

    return buttons_type;
}

GtkWidget *plinia_dialog_message_get_message_area(PliniaDialogMessage *dialog_message)
{
    g_return_val_if_fail(PLINIA_IS_DIALOG_MESSAGE(dialog_message), NULL);

    PliniaDialogMessagePrivate *private = plinia_dialog_message_get_instance_private(dialog_message);

    return GTK_WIDGET(private->gui.message_area);
}
