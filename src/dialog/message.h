#ifndef PLINIA_DIALOG_MESSAGE_H
#define PLINIA_DIALOG_MESSAGE_H
#include <gtk/gtk.h>
#include "dialog.h"

#define PLINIA_TYPE_DIALOG_MESSAGE  plinia_dialog_message_get_type()
G_DECLARE_FINAL_TYPE(PliniaDialogMessage, plinia_dialog_message, PLINIA, DIALOG_MESSAGE, PliniaDialog)

GtkWidget *plinia_dialog_message_new(GtkWindow *parent, const char *title, const char *message, GtkMessageType message_type, GtkButtonsType buttons_type);
void plinia_dialog_message_set_message(PliniaDialogMessage *dialog_message, const char *message);
const char *plinia_dialog_message_get_message(PliniaDialogMessage *dialog_message);
GtkMessageType plinia_dialog_message_get_message_type(PliniaDialogMessage *dialog_message);
GtkButtonsType plinia_dialog_message_get_buttons_type(PliniaDialogMessage *dialog_message);
GtkWidget *plinia_dialog_message_get_message_area(PliniaDialogMessage *dialog_message);

#endif
