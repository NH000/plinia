#ifndef PLINIA_DIALOG_ENTER_PASSWORD_H
#define PLINIA_DIALOG_ENTER_PASSWORD_H
#include <gtk/gtk.h>
#include "dialog.h"

#define PLINIA_TYPE_DIALOG_ENTER_PASSWORD   plinia_dialog_enter_password_get_type()
G_DECLARE_FINAL_TYPE(PliniaDialogEnterPassword, plinia_dialog_enter_password, PLINIA, DIALOG_ENTER_PASSWORD, PliniaDialog)

GtkWidget *plinia_dialog_enter_password_new(GtkWindow *parent, const char *title, const char *message);
const char *plinia_dialog_enter_password_run(PliniaDialogEnterPassword *dialog_enter_password);

#endif
