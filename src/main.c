#include <gio/gio.h>
#include <libintl.h>
#include "application.h"
#include "info.h"

// Sets up internationalization.
static inline void plinia_i18n(const char *locale_dir, const char *text_domain, const char *codeset)
{
    char *_locale_dir = g_filename_from_utf8(locale_dir, -1, NULL, NULL, NULL);
    char *_text_domain = g_filename_from_utf8(text_domain, -1, NULL, NULL, NULL);
    char *_codeset = g_filename_from_utf8(codeset, -1, NULL, NULL, NULL);

    bindtextdomain(_text_domain, _locale_dir);
    bind_textdomain_codeset(_text_domain, _codeset);
    textdomain(_text_domain);

    g_free(_text_domain);
    g_free(_locale_dir);
    g_free(_codeset);
}

int main(int argc, char *argv[])
{
    // Setup internationalization.
    plinia_i18n(PLINIA_LOCALE_DIR, PLINIA_EXEC_NAME, "UTF-8");

    // Create and run the application.
    PliniaApplication *application = plinia_application_new(PLINIA_INFO_IDEN);
    const int status = g_application_run(G_APPLICATION(application), argc, argv);
    g_object_unref(application);

    // Return application status.
    return status;
}
