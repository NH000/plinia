#ifndef PLINIA_AUXILLARY_H
#define PLINIA_AUXILLARY_H
#include <gtk/gtk.h>
#include "document/document.h"

void plinia_auxillary_set_widget_state_on_entry_changed(GtkEntry *entry, GtkWidget *widget);
void plinia_auxillary_click_button_if_sensitive(GtkButton *button);
PliniaDocumentMimeType plinia_auxillary_get_document_mime_type(GFile *file, GError **error);
char *plinia_auxillary_terminate_string(const char *string, gsize length);
gsize plinia_auxillary_get_utf8_string_length(const char *utf8_string);

#endif
