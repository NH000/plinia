#include <gtk/gtk.h>
#include <string.h>
#include "document/document.h"

// Whenever `entry` is changed, `widget` sensitivity is set or unset if entry is non-empty or empty, respectively.
void plinia_auxillary_set_widget_state_on_entry_changed(GtkEntry *entry, GtkWidget *widget)
{
    g_return_if_fail(GTK_IS_ENTRY(entry));
    g_return_if_fail(GTK_IS_WIDGET(widget));

    gtk_widget_set_sensitive(widget, gtk_entry_get_text_length(entry));
}

// Clicks button but only if it is sensitive.
void plinia_auxillary_click_button_if_sensitive(GtkButton *button)
{
    g_return_if_fail(GTK_IS_BUTTON(button));

    if (gtk_widget_get_sensitive(GTK_WIDGET(button)))
        gtk_button_clicked(button);
}

// Determines and returns type of the passed file as one of the supported document formats.
PliniaDocumentMimeType plinia_auxillary_get_document_mime_type(GFile *file, GError **error)
{
    g_return_val_if_fail(G_IS_FILE(file), PLINIA_DOCUMENT_MIME_TYPE_NONE);
    g_return_val_if_fail(error == NULL || *error == NULL, PLINIA_DOCUMENT_MIME_TYPE_NONE);

    PliniaDocumentMimeType ret = PLINIA_DOCUMENT_MIME_TYPE_NONE;

    GFileInfo *file_info = g_file_query_info(file, G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE, G_FILE_QUERY_INFO_NONE, NULL, error);

    if (file_info)
    {
        const char *content_type = g_file_info_get_content_type(file_info);

        if (content_type)
        {
            char *mime_type = g_content_type_get_mime_type(content_type);

            if (mime_type)
            {
                ret =
#ifdef PLINIA_SUPPORT_PDF
                        !g_strcmp0(mime_type, "application/pdf") || !g_strcmp0(mime_type, "application/x-pdf") ? PLINIA_DOCUMENT_MIME_TYPE_PDF :
#endif
                        (
                        PLINIA_DOCUMENT_MIME_TYPE_NONE);

                g_free(mime_type);
            }
        }

        g_object_unref(file_info);
    }

    return ret;
}

// Removes all zero bytes from and appends '\0' at the end of the supplied string.
// Free the returned string after usage.
char *plinia_auxillary_terminate_string(const char *string, gsize length)
{
    g_return_val_if_fail(string != NULL, NULL);

    char *terminated_string = g_malloc0(sizeof(char));
    gsize terminated_string_length = 0;

    for (gsize start = 0, end = 0;end <= length;++end)
    {
        if (!string[end] || end == length)
        {
            terminated_string_length += end - start;
            terminated_string = g_realloc(terminated_string, terminated_string_length + 1);
            strncpy(terminated_string, string + start, end - start);
            terminated_string[terminated_string_length] = '\0';
            start = end + 1;
        }
    }

    return terminated_string;
}

// Returns the number of characters in the supplied NULL-terminated UTF-8 string.
gsize plinia_auxillary_get_utf8_string_length(const char *utf8_string)
{
    g_return_val_if_fail(utf8_string != NULL, 0);
    g_return_val_if_fail(g_utf8_validate(utf8_string, -1, NULL), 0);

    gsize utf8_string_length = 0;
    for (const char *p = utf8_string;*p;p = g_utf8_next_char(p))
        utf8_string_length++;

    return utf8_string_length;
}
