#ifndef PLINIA_TYPES_H
#define PLINIA_TYPES_H
#include <glib-object.h>

/* DUAL PREVIEW MODE */

typedef enum
{
    PLINIA_VIEW_DUAL_MODE_OFF,
    PLINIA_VIEW_DUAL_MODE_EVEN_PAGES_LEFT,
    PLINIA_VIEW_DUAL_MODE_EVEN_PAGES_RIGHT
} PliniaViewDualMode;

#define PLINIA_TYPE_VIEW_DUAL_MODE  plinia_view_dual_mode_get_type()
GType plinia_view_dual_mode_get_type();

/* DUAL PREVIEW DIRECTION MODE */

typedef enum
{
    PLINIA_VIEW_DUAL_DIRECTION_LEFT_TO_RIGHT,
    PLINIA_VIEW_DUAL_DIRECTION_RIGHT_TO_LEFT
} PliniaViewDualDirection;

#define PLINIA_TYPE_VIEW_DUAL_DIRECTION plinia_view_dual_direction_get_type()
GType plinia_view_dual_direction_get_type();

/* APPLICATION OPTIONS */

typedef struct
{
    // View options.
    gboolean show_toolbar;                  // Whether to show toolbar.
    gboolean show_sidebar;                  // Whether to show sidebar.
    gboolean continuous;                    // Is continuous view on?
    PliniaViewDualMode dual;                // Dual view mode.
    PliniaViewDualDirection dual_direction; // Direction of the dual view mode.
    gboolean inverted_colors;               // Should document colors be inverted?
} PliniaApplicationOptionsView;

typedef struct
{
    PliniaApplicationOptionsView view;  // "view": Preview-related settings.
} PliniaApplicationOptions;

#endif
