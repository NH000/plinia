#ifndef PLINIA_CURSOR_H
#define PLINIA_CURSOR_H
#include <gtk/gtk.h>

#define PLINIA_TYPE_CURSOR  plinia_cursor_get_type()
G_DECLARE_FINAL_TYPE(PliniaCursor, plinia_cursor, PLINIA, CURSOR, GObject)

PliniaCursor *plinia_cursor_new(GdkWindow *window);
GdkWindow *plinia_cursor_get_window(PliniaCursor *cursor);
void plinia_cursor_set_cursor_name(PliniaCursor *cursor, const char *cursor_name);
const char *plinia_cursor_get_cursor_name(PliniaCursor *cursor);

#endif
