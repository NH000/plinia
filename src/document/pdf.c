#include <glib/gi18n.h>
#include "pdf.h"
#include "types.h"
#include "../auxillary.h"

/* DOCUMENT PDF ERROR */

G_DEFINE_QUARK(plinia-document-pdf-error-quark, plinia_document_pdf_error)

static const char *error_msg[] =    {
                                        N_("Document is encrypted"),
                                        N_("Failed to open document for writing"),
                                        N_("Could not retrieve document catalog"),
                                        N_("Document is damaged"),
                                        NULL
                                    };

/* DOCUMENT PDF */

struct _PliniaDocumentPDF
{
    GObject parent_instance;

    PopplerDocument *document;          // Underlying Poppler document.

    GFile *file;                        // Associated file.
    PliniaDocumentMimeType mime_type;   // Mime type of the document.
    char *password;                     // Password used to unlock the document.
    GtkTreeStore *catalog;              // Catalog of the document.
    guint n_pages;                      // Total number of pages of the document.
};

G_DEFINE_TYPE_WITH_CODE(PliniaDocumentPDF, plinia_document_pdf, G_TYPE_OBJECT,
    G_IMPLEMENT_INTERFACE(PLINIA_TYPE_DOCUMENT, NULL))

typedef enum
{
    PROP_DOCUMENT = 1,
    N_PROPERTIES
} PliniaDocumentPDFProperty;

static GParamSpec *properties[N_PROPERTIES] = {NULL};

typedef enum
{
    PROP_FILE = N_PROPERTIES,
    PROP_MIME_TYPE,
    PROP_PASSWORD,
    PROP_CATALOG,
    PROP_N_PAGES
} PliniaDocumentPDFPropertyOverride;

static void plinia_document_pdf_construct_catalog(PopplerDocument *document, GtkTreeStore *catalog, GtkTreeIter *catalog_iter_parent, PopplerIndexIter *index_iter);

static void plinia_document_pdf_set_property(GObject *g_object, guint property_id, const GValue *value, GParamSpec *pspec)
{
    PliniaDocumentPDF *instance = PLINIA_DOCUMENT_PDF(g_object);

    switch (property_id)
    {
        case PROP_DOCUMENT:
            if (instance->document)
                g_object_unref(instance->document);

            if ((instance->document = g_value_get_object(value)))
                g_object_ref(instance->document);
            break;
        case PROP_FILE:
            if (instance->file)
                g_object_unref(instance->file);

            if ((instance->file = g_value_get_object(value)))
                g_object_ref(instance->file);
            break;
        case PROP_PASSWORD:
            g_free(instance->password);
            instance->password = g_value_dup_string(value);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(g_object, property_id, pspec);
    }
}

static void plinia_document_pdf_get_property(GObject *g_object, guint property_id, GValue *value, GParamSpec *pspec)
{
    PliniaDocumentPDF *instance = PLINIA_DOCUMENT_PDF(g_object);

    switch (property_id)
    {
        case PROP_DOCUMENT:
            g_value_set_object(value, instance->document);
            break;
        case PROP_FILE:
            g_value_set_object(value, instance->file);
            break;
        case PROP_MIME_TYPE:
            g_value_set_enum(value, instance->mime_type);
            break;
        case PROP_PASSWORD:
            g_value_set_string(value, instance->password);
            break;
        case PROP_CATALOG:
            g_value_set_object(value, instance->catalog);
            break;
        case PROP_N_PAGES:
            g_value_set_uint(value, instance->n_pages);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(g_object, property_id, pspec);
    }
}

static void plinia_document_pdf_constructed(GObject *g_object)
{
    PliniaDocumentPDF *instance = PLINIA_DOCUMENT_PDF(g_object);

    /* CATALOG CONSTRUCTION */

    PopplerIndexIter *index_iter = poppler_index_iter_new(instance->document);

    if (index_iter)
    {
        instance->catalog = PLINIA_DOCUMENT_CREATE_CATALOG;

        plinia_document_pdf_construct_catalog(instance->document, instance->catalog, NULL, index_iter);

        poppler_index_iter_free(index_iter);

        // Delete catalog object if it is empty.
        GtkTreeIter catalog_iter;
        if (!gtk_tree_model_get_iter_first(GTK_TREE_MODEL(instance->catalog), &catalog_iter))
            g_clear_object(&(instance->catalog));
    }

    /* PROPERTIES */

    instance->n_pages = poppler_document_get_n_pages(instance->document);

    G_OBJECT_CLASS(plinia_document_pdf_parent_class)->constructed(g_object);
}

static void plinia_document_pdf_finalize(GObject *g_object)
{
    PliniaDocumentPDF *instance = PLINIA_DOCUMENT_PDF(g_object);

    if (instance->catalog)
        g_object_unref(instance->catalog);
    g_free(instance->password);
    if (instance->file)
        g_object_unref(instance->file);
    if (instance->document)
        g_object_unref(instance->document);

    G_OBJECT_CLASS(plinia_document_pdf_parent_class)->finalize(g_object);
}

static void plinia_document_pdf_class_init(PliniaDocumentPDFClass *class)
{
    GObjectClass *g_object_class = G_OBJECT_CLASS(class);
    g_object_class->set_property = plinia_document_pdf_set_property;
    g_object_class->get_property = plinia_document_pdf_get_property;
    g_object_class->constructed = plinia_document_pdf_constructed;
    g_object_class->finalize = plinia_document_pdf_finalize;

    properties[PROP_DOCUMENT] = g_param_spec_object("document", _("Poppler document"), _("Underlying Poppler document."), POPPLER_TYPE_DOCUMENT, G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

    g_object_class_install_properties(g_object_class, N_PROPERTIES, properties);

    g_object_class_override_property(g_object_class, PROP_FILE, "file");
    g_object_class_override_property(g_object_class, PROP_MIME_TYPE, "mime-type");
    g_object_class_override_property(g_object_class, PROP_PASSWORD, "password");
    g_object_class_override_property(g_object_class, PROP_CATALOG, "catalog");
    g_object_class_override_property(g_object_class, PROP_N_PAGES, "n-pages");
}

static void plinia_document_pdf_init(PliniaDocumentPDF *instance)
{
    instance->mime_type = PLINIA_DOCUMENT_MIME_TYPE_PDF;
}

// Creates tree model that contains document destinations.
static void plinia_document_pdf_construct_catalog(PopplerDocument *document, GtkTreeStore *catalog, GtkTreeIter *catalog_iter_parent, PopplerIndexIter *index_iter)
{
    g_return_if_fail(POPPLER_IS_DOCUMENT(document));
    g_return_if_fail(!catalog || GTK_IS_TREE_STORE(catalog));
    g_return_if_fail(index_iter != NULL);

    do
    {
        PopplerAction *action = poppler_index_iter_get_action(index_iter);

        if (action->type == POPPLER_ACTION_NAMED)
        {
            const char *title = action->named.title;
            PopplerDest *dest = poppler_document_find_dest(document, action->named.named_dest);

            GtkTreeIter catalog_iter;
            gtk_tree_store_append(catalog, &catalog_iter, catalog_iter_parent);
            gtk_tree_store_set(catalog, &catalog_iter, 0, title, 1, poppler_index_iter_is_open(index_iter), 2, dest->page_num, 3, dest->left, 4, dest->bottom, 5, dest->right, 6, dest->top, 7, dest->zoom, -1);

            poppler_dest_free(dest);

            PopplerIndexIter *index_iter_child = poppler_index_iter_get_child(index_iter);

            if (index_iter_child)
            {
                plinia_document_pdf_construct_catalog(document, catalog, &catalog_iter, index_iter_child);
                poppler_index_iter_free(index_iter_child);
            }
        }

        poppler_action_free(action);
    } while (poppler_index_iter_next(index_iter));
}

/* API */

PliniaDocument *plinia_document_pdf_new(GFile *file, const char *password, GError **error)
{
    g_return_val_if_fail(G_IS_FILE(file), NULL);
    g_return_val_if_fail(error == NULL || *error == NULL, NULL);

    // Try to open Poppler document from the supplied file.
    GError *error_poppler = NULL;
    PopplerDocument *document = poppler_document_new_from_gfile(file, password, NULL, &error_poppler);
    if (!document)
    {
        const PliniaDocumentPDFError error_code = error_poppler->code == POPPLER_ERROR_ENCRYPTED ? PLINIA_DOCUMENT_PDF_ERROR_LOCKED
                                                  : (error_poppler->code == POPPLER_ERROR_OPEN_FILE ? PLINIA_DOCUMENT_PDF_ERROR_OPEN_WRITE
                                                  : (error_poppler->code == POPPLER_ERROR_BAD_CATALOG ? PLINIA_DOCUMENT_PDF_ERROR_BAD_CATALOG
                                                  : (error_poppler->code == POPPLER_ERROR_DAMAGED ? PLINIA_DOCUMENT_PDF_ERROR_DAMAGED
                                                  : PLINIA_DOCUMENT_PDF_ERROR_FAILURE)));

        g_error_free(error_poppler);
        g_set_error(error, PLINIA_DOCUMENT_PDF_ERROR, error_code, _(error_msg[error_code]));

        return NULL;
    }

    PliniaDocument *plinia_document = g_object_new(PLINIA_TYPE_DOCUMENT_PDF, "document", document, "file", file, "password", password, NULL);

    // Unreference Poppler document since it was referenced by property setter.
    g_object_unref(document);

    return plinia_document;
}

PopplerDocument *plinia_document_pdf_get_document(PliniaDocumentPDF *document_pdf)
{
    g_return_val_if_fail(PLINIA_IS_DOCUMENT_PDF(document_pdf), NULL);

    PopplerDocument *document;
    g_object_get(document_pdf, "document", &document, NULL);

    return document;
}
