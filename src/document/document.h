#ifndef PLINIA_DOCUMENT_H
#define PLINIA_DOCUMENT_H
#include <gtk/gtk.h>
#include "types.h"

// Catalog columns are:
//  0: Location title.
//  1: Should the location subtree be opened by default.
//  2: Page number.
//  3: Left coordinate in the page.
//  4: Bottom coordinate in the page.
//  5: Right coordinate in the page.
//  6: Top coordinate in the page.
//  7: Zoom level.
#define PLINIA_DOCUMENT_CREATE_CATALOG  gtk_tree_store_new(8, G_TYPE_STRING, G_TYPE_BOOLEAN, G_TYPE_UINT, G_TYPE_DOUBLE, G_TYPE_DOUBLE, G_TYPE_DOUBLE, G_TYPE_DOUBLE, G_TYPE_DOUBLE)

#define PLINIA_TYPE_DOCUMENT    plinia_document_get_type()
G_DECLARE_INTERFACE(PliniaDocument, plinia_document, PLINIA, DOCUMENT, GObject)

struct _PliniaDocumentInterface
{
    GTypeInterface parent_iface;
};

GFile *plinia_document_get_file(PliniaDocument *document);
PliniaDocumentMimeType plinia_document_get_mime_type(PliniaDocument *document);
const char *plinia_document_get_password(PliniaDocument *document);
GtkTreeStore *plinia_document_get_catalog(PliniaDocument *document);
guint plinia_document_get_n_pages(PliniaDocument *document);

#endif
