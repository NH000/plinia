#ifndef PLINIA_DOCUMENT_PDF_H
#define PLINIA_DOCUMENT_PDF_H
#include <gio/gio.h>
#include <poppler.h>
#include "document.h"

#define PLINIA_TYPE_DOCUMENT_PDF    plinia_document_pdf_get_type()
G_DECLARE_FINAL_TYPE(PliniaDocumentPDF, plinia_document_pdf, PLINIA, DOCUMENT_PDF, GObject)

PliniaDocument *plinia_document_pdf_new(GFile *file, const char *password, GError **error);
PopplerDocument *plinia_document_pdf_get_document(PliniaDocumentPDF *document_pdf);

#endif
