#ifndef PLINIA_TYPES_DOCUMENT_H
#define PLINIA_TYPES_DOCUMENT_H

/* DOCUMENT MIME TYPE */

typedef enum
{
    PLINIA_DOCUMENT_MIME_TYPE_NONE, // Unknown or unsupported format.
#ifdef PLINIA_SUPPORT_PDF
    PLINIA_DOCUMENT_MIME_TYPE_PDF   // PDF.
#endif
} PliniaDocumentMimeType;

#define PLINIA_TYPE_DOCUMENT_MIME_TYPE  plinia_document_mime_type_get_type()
GType plinia_document_mime_type_get_type();

/* DOCUMENT PDF ERROR */

#ifdef PLINIA_SUPPORT_PDF
typedef enum
{
    PLINIA_DOCUMENT_PDF_ERROR_LOCKED,
    PLINIA_DOCUMENT_PDF_ERROR_OPEN_WRITE,
    PLINIA_DOCUMENT_PDF_ERROR_BAD_CATALOG,
    PLINIA_DOCUMENT_PDF_ERROR_DAMAGED,
    PLINIA_DOCUMENT_PDF_ERROR_FAILURE
} PliniaDocumentPDFError;

#define PLINIA_DOCUMENT_PDF_ERROR   plinia_document_pdf_error_quark()
GQuark plinia_document_pdf_error_quark();
#endif

#endif
