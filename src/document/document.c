#include <glib/gi18n.h>
#include "document.h"

/* DOCUMENT MIME TYPE */

GType plinia_document_mime_type_get_type()
{
    static GType type = 0;

    if (type == 0)
    {
        static const GEnumValue values[] =  {
                                                {PLINIA_DOCUMENT_MIME_TYPE_NONE, "PLINIA_DOCUMENT_MIME_TYPE_NONE", "none"},
#ifdef PLINIA_SUPPORT_PDF
                                                {PLINIA_DOCUMENT_MIME_TYPE_PDF, "PLINIA_DOCUMENT_MIME_TYPE_PDF", "pdf"},
#endif
                                                {0, NULL, NULL}
                                            };

        type = g_enum_register_static("PliniaDocumentMimeType", values);
    }

    return type;
}

/* DOCUMENT */

G_DEFINE_INTERFACE(PliniaDocument, plinia_document, G_TYPE_OBJECT)

static void plinia_document_default_init(PliniaDocumentInterface *iface)
{
    g_object_interface_install_property(iface, g_param_spec_object("file", _("File"), _("Associated file."), G_TYPE_FILE, G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
    g_object_interface_install_property(iface, g_param_spec_enum("mime-type", _("Mime type"), _("Mime type of the document."), PLINIA_TYPE_DOCUMENT_MIME_TYPE, PLINIA_DOCUMENT_MIME_TYPE_NONE, G_PARAM_READABLE));
    g_object_interface_install_property(iface, g_param_spec_string("password", _("Password"), _("Document password."), NULL, G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
    g_object_interface_install_property(iface, g_param_spec_object(("catalog"), _("Catalog"), _("Document catalog."), GTK_TYPE_TREE_STORE, G_PARAM_READABLE));
    g_object_interface_install_property(iface, g_param_spec_uint("n-pages", _("N pages"), _("Total number of pages of the document."), 0, G_MAXUINT, 0, G_PARAM_READABLE));
}

/* API */

GFile *plinia_document_get_file(PliniaDocument *document)
{
    g_return_val_if_fail(PLINIA_IS_DOCUMENT(document), NULL);

    GFile *file;
    g_object_get(document, "file", &file, NULL);

    return file;
}

PliniaDocumentMimeType plinia_document_get_mime_type(PliniaDocument *document)
{
    g_return_val_if_fail(PLINIA_IS_DOCUMENT(document), PLINIA_DOCUMENT_MIME_TYPE_NONE);

    PliniaDocumentMimeType mime_type;
    g_object_get(document, "mime-type", &mime_type, NULL);

    return mime_type;
}

const char *plinia_document_get_password(PliniaDocument *document)
{
    g_return_val_if_fail(PLINIA_IS_DOCUMENT(document), NULL);

    const char *password;
    g_object_get(document, "password", &password, NULL);

    return password;
}

GtkTreeStore *plinia_document_get_catalog(PliniaDocument *document)
{
    g_return_val_if_fail(PLINIA_IS_DOCUMENT(document), NULL);

    GtkTreeStore *catalog;
    g_object_get(document, "catalog", &catalog, NULL);

    return catalog;
}

guint plinia_document_get_n_pages(PliniaDocument *document)
{
    g_return_val_if_fail(PLINIA_IS_DOCUMENT(document), 0);

    guint n_pages;
    g_object_get(document, "n-pages", &n_pages, NULL);

    return n_pages;
}
