#include <glib/gi18n.h>
#include <string.h>
#include "application.h"
#include "info.h"
#include "auxillary.h"
#include "dialog/message.h"
#include "dialog/enter_password.h"
#ifdef PLINIA_SUPPORT_PDF
#include "document/pdf.h"
#endif
#include "window/window.h"

typedef struct
{
    char **password;                   // "password": Password array used to unlock supplied files.
} PliniaApplicationArgumentsDocument;

typedef struct
{
    PliniaApplicationArgumentsDocument document; // "document": Document-related arguments.
} PliniaApplicationArguments;

typedef struct
{
    char *config_file_path;                 // Path to the configuration file.
    PliniaApplicationArguments arguments;   // Command-line arguments holder.
    PliniaApplicationOptions options;       // Application options.
} PliniaApplicationPrivate;

struct _PliniaApplication
{
    GtkApplication parent_instance;

    PliniaDocument *document;   // Currently opened document.
};

G_DEFINE_TYPE_WITH_PRIVATE(PliniaApplication, plinia_application, GTK_TYPE_APPLICATION)

typedef enum
{
    PROP_DOCUMENT = 1,
    N_PROPERTIES
} PliniaApplicationProperty;

static GParamSpec *properties[N_PROPERTIES] = {NULL};

static void plinia_application_set_property(GObject *g_object, guint property_id, const GValue *value, GParamSpec *pspec)
{
    PliniaApplication *instance = PLINIA_APPLICATION(g_object);

    switch (property_id)
    {
        case PROP_DOCUMENT:
            if (instance->document)
                g_object_unref(instance->document);

            if ((instance->document = g_value_get_object(value)))
                g_object_ref(instance->document);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(g_object, property_id, pspec);
    }
}

static void plinia_application_get_property(GObject *g_object, guint property_id, GValue *value, GParamSpec *pspec)
{
    PliniaApplication *instance = PLINIA_APPLICATION(g_object);

    switch (property_id)
    {
        case PROP_DOCUMENT:
            g_value_set_object(value, instance->document);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(g_object, property_id, pspec);
    }
}

static void plinia_application_finalize(GObject *g_object)
{
    PliniaApplication *instance = PLINIA_APPLICATION(g_object);
    PliniaApplicationPrivate *private = plinia_application_get_instance_private(instance);

    g_free(private->config_file_path);

    /* RELEASE COMMAND-LINE ARGUMENT HOLDER */

    g_strfreev(private->arguments.document.password);

    /* RELEASE DOCUMENT */

    if (instance->document)
        g_object_unref(instance->document);

    G_OBJECT_CLASS(plinia_application_parent_class)->finalize(g_object);
}

// Acts on options that do not result in application activation.
static int plinia_application_handle_local_options(GApplication *g_application, GVariantDict *dictionary)
{
    int ret = -1;

    if (g_variant_dict_contains(dictionary, "version"))
    {
        g_print(_("Version: %s\n"), PLINIA_INFO_VERS);
        ret = 0;
    }

    if (g_variant_dict_contains(dictionary, "license"))
    {
        g_print(_("License: %s\n"), PLINIA_INFO_LICE);
        ret = 0;
    }

    return ret;
}

// Attempts to open the first supplied document file using the supplied password (if there is one)
// and spawns process to handle each subsequent file, preserving supplied passwords.
static void plinia_application_open(GApplication *g_application, GFile **files, int n_files, const char *hint)
{
    PliniaApplication *instance = PLINIA_APPLICATION(g_application);
    PliniaApplicationPrivate *private = plinia_application_get_instance_private(instance);

    /* DOCUMENT OPENING */

    GError *error = NULL;
    const PliniaDocumentMimeType mime_type = plinia_auxillary_get_document_mime_type(*files, &error);

    if (mime_type == PLINIA_DOCUMENT_MIME_TYPE_NONE)
    {
        char *message = g_strdup_printf(error ? error->message : _("File \"%s\" is of unsupported type."), g_file_peek_path(*files));
        if (error)
            g_error_free(error);
        GtkWidget *dialog_message = plinia_dialog_message_new(NULL, _("Unsuccessful retrieval of file information"), message, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK);
        g_free(message);

        gtk_dialog_run(GTK_DIALOG(dialog_message));
    }
    else
    {
        char *password = private->arguments.document.password ? *private->arguments.document.password : NULL;

open_doc:;
        GError *error = NULL;
        PliniaDocument *document =
#ifdef PLINIA_SUPPORT_PDF
                                    mime_type == PLINIA_DOCUMENT_MIME_TYPE_PDF ? plinia_document_pdf_new(*files, password, &error) :
#endif
                                    (
                                    NULL);

        if (private->arguments.document.password && password != *private->arguments.document.password)
            g_free(password);

        if (document)
        {
            g_object_set(instance, "document", document, NULL);
            g_object_unref(document);
        }
        else if (
#ifdef PLINIA_SUPPORT_PDF
            (error->domain == PLINIA_DOCUMENT_PDF_ERROR && error->code == PLINIA_DOCUMENT_PDF_ERROR_LOCKED) ||
#endif
            FALSE
                )
        {
            g_error_free(error);

            char *message = g_strdup_printf(password ? _("Wrong password for unlocking document \"%s\"!\nTry entering another password.") : _("Document \"%s\" is locked.\nPlease enter password to unlock it."), g_file_peek_path(*files));
            GtkWidget *dialog_enter_password = plinia_dialog_enter_password_new(NULL, _("Document locked"), message);
            g_free(message);

            if ((password = g_strdup(plinia_dialog_enter_password_run(PLINIA_DIALOG_ENTER_PASSWORD(dialog_enter_password)))))
            {
                gtk_widget_destroy(dialog_enter_password);
                goto open_doc;
            }

            gtk_widget_destroy(dialog_enter_password);
        }
        else
        {
            char *message = g_strdup_printf(_("Failed to open document \"%s\": %s"), g_file_peek_path(*files), error->message);
            g_error_free(error);
            GtkWidget *dialog_message = plinia_dialog_message_new(NULL, _("Opening document failed"), message, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK);
            g_free(message);

            gtk_dialog_run(GTK_DIALOG(dialog_message));
        }
    }

    /* SPAWN PROCESS TO HANDLE EACH SUBSEQUENT FILE */

    guint password_index = 1;

#define PLINIA_APPLICATION_SPAWN_PROCESS_FOR_DOCUMENT_ERROR {                                                                                               \
                                                                g_printerr(_("Could not spawn process for file \"%s\": %s\n"), argv[1], error->message);    \
                                                                g_error_free(error);                                                                        \
                                                            }

    char * const exec_name = n_files > 1 ? g_filename_from_utf8(PLINIA_EXEC_NAME, -1, NULL, NULL, NULL) : NULL;
    for (guint i = 1;i < n_files;++i)
    {
        GError *error = NULL;

        if (!private->arguments.document.password || !private->arguments.document.password[password_index])
        {
            char *argv[3] = {exec_name, g_file_get_path(files[i]), NULL};

            if (!g_spawn_async(NULL, argv, NULL, G_SPAWN_SEARCH_PATH, NULL, NULL, NULL, &error))
                PLINIA_APPLICATION_SPAWN_PROCESS_FOR_DOCUMENT_ERROR

            g_free(argv[1]);
        }
        else
        {
            char *argv[5] = {PLINIA_EXEC_NAME, "-p", private->arguments.document.password[password_index++], g_file_get_path(files[i]), NULL};

            if (!g_spawn_async(NULL, argv, NULL, G_SPAWN_SEARCH_PATH, NULL, NULL, NULL, &error))
                PLINIA_APPLICATION_SPAWN_PROCESS_FOR_DOCUMENT_ERROR

            g_free(argv[3]);
        }
    }
    g_free(exec_name);

#undef PLINIA_APPLICATION_SPAWN_PROCESS_FOR_DOCUMENT_ERROR

    // Run application if document was successfully opened.
    if (instance->document)
        g_application_activate(g_application);
}

// Creates application window and displays it.
// After this function main loop runs.
static void plinia_application_activate(GApplication *g_application)
{
    PliniaApplication *application = PLINIA_APPLICATION(g_application);
    PliniaApplicationPrivate *private = plinia_application_get_instance_private(application);

    /* APPLICATION WINDOW */

    // Create application window.
    GtkWidget *window = plinia_window_new(application);

    // If file was opened through the command-line, set window state to match.
    if (application->document)
    {
        // Set window title to the full path of the opened document.
        GFile *file = plinia_document_get_file(application->document);
        gtk_window_set_title(GTK_WINDOW(window), g_file_peek_path(file));
        g_object_unref(file);

        // If the document has more than one page, enable "Next Page" action of the window.
        plinia_window_set_navigation_enabled(PLINIA_WINDOW(window), TRUE, plinia_document_get_n_pages(application->document) == 1);
    }
    else    // Otherwise, disable associated window elements.
    {
        plinia_window_set_file_enabled(PLINIA_WINDOW(window), FALSE);
        plinia_window_set_navigation_enabled(PLINIA_WINDOW(window), TRUE, TRUE);
        plinia_window_set_bookmark_enabled(PLINIA_WINDOW(window), FALSE);
    }

    // Some window functionality should be disabled on startup regardless if a document was opened or not.
    plinia_window_set_find_prev_and_next_enabled(PLINIA_WINDOW(window), FALSE);
    plinia_window_set_copy_enabled(PLINIA_WINDOW(window), FALSE);
    plinia_window_set_history_enabled(PLINIA_WINDOW(window), TRUE, TRUE);

    // Add shortcuts to the application for window actions.
    plinia_window_add_accelerators(PLINIA_WINDOW(window));

    // Configure window from application options.
    plinia_window_configure_from_options(PLINIA_WINDOW(window), &(private->options));

    /* DISPLAYING */

    // Show application window.
    gtk_widget_show(window);
}

static void plinia_application_class_init(PliniaApplicationClass *class)
{
    GObjectClass *g_object_class = G_OBJECT_CLASS(class);
    g_object_class->set_property = plinia_application_set_property;
    g_object_class->get_property = plinia_application_get_property;
    g_object_class->finalize = plinia_application_finalize;

    GApplicationClass *g_application_class = G_APPLICATION_CLASS(class);
    g_application_class->handle_local_options = plinia_application_handle_local_options;
    g_application_class->open = plinia_application_open;
    g_application_class->activate = plinia_application_activate;

    properties[PROP_DOCUMENT] = g_param_spec_object("document", _("Document"), _("Currently opened document."), PLINIA_TYPE_DOCUMENT, G_PARAM_READWRITE);

    g_object_class_install_properties(g_object_class, N_PROPERTIES, properties);
}

static void plinia_application_init(PliniaApplication *instance)
{
    PliniaApplicationPrivate *private = plinia_application_get_instance_private(instance);

    // Set configuration file path.
    {
        char *config_file_path = g_filename_from_utf8(PLINIA_EXEC_NAME ".ini", -1, NULL, NULL, NULL);
        private->config_file_path = g_build_filename(g_get_user_config_dir(), config_file_path, NULL);
        g_free(config_file_path);
    }

    /* SPECIFY COMMAND-LINE ARGUMENTS */

    GApplication *g_application = G_APPLICATION(instance);

    // Main options.
    {
        GOptionEntry entries[] =    {
                                        {
                                            "version",
                                            '\0',
                                            G_OPTION_FLAG_NONE,
                                            G_OPTION_ARG_NONE,
                                            NULL,
                                            _("Show program version"),
                                            NULL
                                        },
                                        {
                                            "license",
                                            '\0',
                                            G_OPTION_FLAG_NONE,
                                            G_OPTION_ARG_NONE,
                                            NULL,
                                            _("Show program license"),
                                            NULL
                                        },
                                        {
                                            NULL,
                                            '\0',
                                            G_OPTION_FLAG_NONE,
                                            G_OPTION_ARG_NONE,
                                            NULL,
                                            NULL,
                                            NULL
                                        }
                                    };

        g_application_add_main_option_entries(g_application, entries);
    }

    // Document options.
    {
        GOptionEntry entries[] =    {
                                        {
                                            "password",
                                            'p',
                                            G_OPTION_FLAG_NONE,
                                            G_OPTION_ARG_STRING_ARRAY,
                                            &(private->arguments.document.password),
                                            _("Password array for unlocking supplied documents"),
                                            _("PASSWORD")
                                        },
                                        {
                                            NULL,
                                            '\0',
                                            G_OPTION_FLAG_NONE,
                                            G_OPTION_ARG_NONE,
                                            NULL,
                                            NULL,
                                            NULL
                                        }
                                    };

        GOptionGroup *group = g_option_group_new("document", _("Document Options:"), _("Show document options"), NULL, NULL);
        g_option_group_add_entries(group, entries);
        g_application_add_option_group(g_application, group);
    }

    // Set help menu descriptions.
    g_application_set_option_context_parameter_string(g_application, _("[FILE\u2026]"));

    /* CONFIGURATION OPTIONS */

    GKeyFile *key_file = g_key_file_new();
    g_key_file_load_from_file(key_file, private->config_file_path, G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS, NULL);

#define PLINIA_APPLICATION_OPTIONS_GET(type, group, key, def)  (g_key_file_has_key(key_file, group, key, NULL) ? g_key_file_get_ ## type(key_file, group, key, NULL) : def)

    // View section.
    private->options.view.show_toolbar = PLINIA_APPLICATION_OPTIONS_GET(boolean, "view", "show-toolbar", FALSE);
    private->options.view.show_sidebar = PLINIA_APPLICATION_OPTIONS_GET(boolean, "view", "show-sidebar", FALSE);
    private->options.view.continuous = PLINIA_APPLICATION_OPTIONS_GET(boolean, "view", "continuous", FALSE);
    private->options.view.dual = PLINIA_APPLICATION_OPTIONS_GET(integer, "view", "dual", PLINIA_VIEW_DUAL_MODE_OFF);
    private->options.view.dual_direction = PLINIA_APPLICATION_OPTIONS_GET(integer, "view", "dual-direction", PLINIA_VIEW_DUAL_DIRECTION_LEFT_TO_RIGHT);
    private->options.view.inverted_colors = PLINIA_APPLICATION_OPTIONS_GET(boolean, "view", "inverted-colors", FALSE);

#undef PLINIA_APPLICATION_OPTIONS_GET

    g_key_file_free(key_file);
}

/* API */

PliniaApplication *plinia_application_new(const char *application_id)
{
    return g_object_new(PLINIA_TYPE_APPLICATION, "application-id", application_id, "flags", G_APPLICATION_NON_UNIQUE | G_APPLICATION_HANDLES_OPEN, NULL);
}

void plinia_application_set_document(PliniaApplication *application, PliniaDocument *document)
{
    g_return_if_fail(PLINIA_IS_APPLICATION(application));
    g_return_if_fail(!document || PLINIA_IS_DOCUMENT(document));

    g_object_set(application, "document", document, NULL);
}

PliniaDocument *plinia_application_get_document(PliniaApplication *application)
{
    g_return_val_if_fail(PLINIA_IS_APPLICATION(application), NULL);

    PliniaDocument *document;
    g_object_get(application, "document", &document, NULL);

    return document;
}

const PliniaApplicationOptions *plinia_application_options_get(PliniaApplication *application)
{
    g_return_val_if_fail(PLINIA_IS_APPLICATION(application), NULL);

    PliniaApplicationPrivate *private = plinia_application_get_instance_private(application);

    return &(private->options);
}

void plinia_application_options_set(PliniaApplication *application, const PliniaApplicationOptions *application_options)
{
    g_return_if_fail(PLINIA_IS_APPLICATION(application));
    g_return_if_fail(application_options != NULL);

    PliniaApplicationPrivate *private = plinia_application_get_instance_private(application);

    memcpy(&(private->options), application_options, sizeof(PliniaApplicationOptions));
}

void plinia_application_options_update(PliniaApplication *application)
{
    g_return_if_fail(PLINIA_IS_APPLICATION(application));

    PliniaApplicationPrivate *private = plinia_application_get_instance_private(application);

    GKeyFile *key_file = g_key_file_new();

    // View section.
    g_key_file_set_boolean(key_file, "view", "show-toolbar", private->options.view.show_toolbar);
    g_key_file_set_boolean(key_file, "view", "show-sidebar", private->options.view.show_sidebar);
    g_key_file_set_boolean(key_file, "view", "continuous", private->options.view.continuous);
    g_key_file_set_integer(key_file, "view", "dual", private->options.view.dual);
    g_key_file_set_integer(key_file, "view", "dual-direction", private->options.view.dual_direction);
    g_key_file_set_boolean(key_file, "view", "inverted-colors", private->options.view.inverted_colors);

    GError *error = NULL;

    if (!g_key_file_save_to_file(key_file, private->config_file_path, &error))
    {
        g_printerr(_("Could not write to configuration file: %s\n"), error->message);
        g_error_free(error);
    }

    g_key_file_free(key_file);
}
