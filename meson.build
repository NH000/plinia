project('plinia',
        'c',
        default_options : ['c_std=c99', 'prefix=/usr'],
        license : 'GPL-3.0-or-later',
        version : '0.5')

exec_name = get_option('exec_name')
locale_dir = get_option('prefix') / get_option('localedir')
identifier = get_option('identifier')
support = get_option('support')
zoom_level_min = get_option('zoom_level_min')
zoom_level_max = get_option('zoom_level_max')
zoom_level_def = get_option('zoom_level_def')

assert(exec_name != '')
assert(identifier != '')
assert(zoom_level_min > 0, 'Minimum zoom level value is less than or equal to 0')
assert(zoom_level_max >= zoom_level_min, 'Maximum zoom level value is lesser than the minumum zoom level value')
assert(zoom_level_def >= zoom_level_min and zoom_level_def <= zoom_level_max, 'Default zoom level value is not inbetween minimum and maximum zoom level values')

src_dir = 'src'

deps = [dependency('gtk+-3.0')]
srcs = [src_dir / 'main.c', src_dir / 'auxillary.c', src_dir / 'cursor.c', src_dir / 'application.c', src_dir / 'view.c',
        src_dir / 'window/window.c', src_dir / 'window/menubar.c', src_dir / 'window/toolbar.c', src_dir / 'window/separator_invisible_tool_item.c', src_dir / 'window/spacer_tool_item.c', src_dir / 'window/navigator.c', src_dir / 'window/zoom_level_chooser.c',
        src_dir / 'document/document.c',
        src_dir / 'dialog/dialog.c', src_dir / 'dialog/message.c', src_dir / 'dialog/enter_password.c']

add_project_arguments('-DPLINIA_EXEC_NAME="' + exec_name + '"',
                      '-DPLINIA_INFO_IDEN="' + identifier + '"',
                      '-DPLINIA_LOCALE_DIR="' + locale_dir + '"',
                      '-DPLINIA_ZOOM_LEVEL_MIN=' + zoom_level_min.to_string(),
                      '-DPLINIA_ZOOM_LEVEL_MAX=' + zoom_level_max.to_string(),
                      '-DPLINIA_ZOOM_LEVEL_DEF=' + zoom_level_def.to_string(),
                      language : 'c')

if support.contains('pdf')
    add_project_arguments('-DPLINIA_SUPPORT_PDF', language : 'c')
    deps += dependency('poppler-glib')
    srcs += src_dir / 'document/pdf.c'
endif

symbols = configuration_data({
                                'PLINIA_EXEC_NAME' : exec_name,
                                'PLINIA_BIN_DIR' : get_option('prefix') / get_option('bindir')
                             })

configure_file(input : 'com.gitlab.NH000.Plinia.desktop.in', output : identifier + '.desktop', configuration : symbols)

executable(exec_name, srcs, dependencies : deps, install : true)

install_data('icon.svg', install_dir : get_option('prefix') / get_option('datadir') / 'icons/hicolor/scalable/apps', rename : exec_name + '.svg', install_tag : 'runtime')

install_data(meson.current_build_dir() / identifier + '.desktop', install_dir : get_option('prefix') / get_option('datadir') / 'applications', install_tag : 'launcher')
