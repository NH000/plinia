EXEC_NAME := plinia
IDENTIFIER := com.gitlab.NH000.Plinia
PREFIX_DIR := /usr
BIN_DIR := bin
LOCALE_DIR := share/locale

# Deletes file $(2) from directory $(1) and then deletes all empty parent directories in bottom-to-top order.
delete_file_and_empty_parents = if test -d '$(1)'; then rm -f '$(1)/$(2)' && rmdir -p --ignore-fail-on-non-empty '$(1)'; fi

.PHONY: help uninstall

help:
	$(info VARIABLES)
	$(info ================================================================================)
	$(info EXEC_NAME:   Name of the executable.)
	$(info IDENTIFIER:  Application identifier.)
	$(info PREFIX_DIR:  Install directory prefix.)
	$(info BIN_DIR:     Binary subdirectory of installation prefix.)
	$(info LOCALE_DIR:  Locale subdirectory of installation prefix.)
	$(info )
	$(info RULES)
	$(info ================================================================================)
	$(info help:         Display this help menu.)
	$(info uninstall:    Uninstall the program and its data.)

uninstall:
	rm -f '$(PREFIX_DIR)/$(BIN_DIR)/$(EXEC_NAME)'
	$(foreach po,$(wildcard po/*.po),$(call delete_file_and_empty_parents,$(PREFIX_DIR)/$(LOCALE_DIR)/$(patsubst po/%.po,%,$(po))/LC_MESSAGES,$(EXEC_NAME).mo);)
	rm -f '$(PREFIX_DIR)/$(DATA_DIR)/icons/hicolor/scalable/apps/$(EXEC_NAME).svg'
	rm -f '$(PREFIX_DIR)/$(DATA_DIR)/applications/$(IDENTIFIER).desktop'
